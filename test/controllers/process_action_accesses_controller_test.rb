require 'test_helper'

class ProcessActionAccessesControllerTest < ActionController::TestCase
  setup do
    @process_action_access = process_action_accesses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:process_action_accesses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create process_action_access" do
    assert_difference('ProcessActionAccess.count') do
      post :create, process_action_access: { employee_id: @process_action_access.employee_id, end_date: @process_action_access.end_date, is_close: @process_action_access.is_close, process_action_id: @process_action_access.process_action_id, process_template_id: @process_action_access.process_template_id, staff_unit_id: @process_action_access.staff_unit_id, start_date: @process_action_access.start_date }
    end

    assert_redirected_to process_action_access_path(assigns(:process_action_access))
  end

  test "should show process_action_access" do
    get :show, id: @process_action_access
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @process_action_access
    assert_response :success
  end

  test "should update process_action_access" do
    patch :update, id: @process_action_access, process_action_access: { employee_id: @process_action_access.employee_id, end_date: @process_action_access.end_date, is_close: @process_action_access.is_close, process_action_id: @process_action_access.process_action_id, process_template_id: @process_action_access.process_template_id, staff_unit_id: @process_action_access.staff_unit_id, start_date: @process_action_access.start_date }
    assert_redirected_to process_action_access_path(assigns(:process_action_access))
  end

  test "should destroy process_action_access" do
    assert_difference('ProcessActionAccess.count', -1) do
      delete :destroy, id: @process_action_access
    end

    assert_redirected_to process_action_accesses_path
  end
end
