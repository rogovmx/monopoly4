require 'test_helper'

class AgreementAccessesControllerTest < ActionController::TestCase
  setup do
    @agreement_access = agreement_accesses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:agreement_accesses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create agreement_access" do
    assert_difference('AgreementAccess.count') do
      post :create, agreement_access: { agreement_id: @agreement_access.agreement_id, is_close: @agreement_access.is_close, staff_unit_id: @agreement_access.staff_unit_id }
    end

    assert_redirected_to agreement_access_path(assigns(:agreement_access))
  end

  test "should show agreement_access" do
    get :show, id: @agreement_access
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @agreement_access
    assert_response :success
  end

  test "should update agreement_access" do
    patch :update, id: @agreement_access, agreement_access: { agreement_id: @agreement_access.agreement_id, is_close: @agreement_access.is_close, staff_unit_id: @agreement_access.staff_unit_id }
    assert_redirected_to agreement_access_path(assigns(:agreement_access))
  end

  test "should destroy agreement_access" do
    assert_difference('AgreementAccess.count', -1) do
      delete :destroy, id: @agreement_access
    end

    assert_redirected_to agreement_accesses_path
  end
end
