require 'test_helper'

class ContractSubjectRolesControllerTest < ActionController::TestCase
  setup do
    @contract_subject_role = contract_subject_roles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contract_subject_roles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contract_subject_role" do
    assert_difference('ContractSubjectRole.count') do
      post :create, contract_subject_role: { contract_subject_id: @contract_subject_role.contract_subject_id, descr: @contract_subject_role.descr, id: @contract_subject_role.id, is_close: @contract_subject_role.is_close, name: @contract_subject_role.name }
    end

    assert_redirected_to contract_subject_role_path(assigns(:contract_subject_role))
  end

  test "should show contract_subject_role" do
    get :show, id: @contract_subject_role
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contract_subject_role
    assert_response :success
  end

  test "should update contract_subject_role" do
    put :update, id: @contract_subject_role, contract_subject_role: { contract_subject_id: @contract_subject_role.contract_subject_id, descr: @contract_subject_role.descr, id: @contract_subject_role.id, is_close: @contract_subject_role.is_close, name: @contract_subject_role.name }
    assert_redirected_to contract_subject_role_path(assigns(:contract_subject_role))
  end

  test "should destroy contract_subject_role" do
    assert_difference('ContractSubjectRole.count', -1) do
      delete :destroy, id: @contract_subject_role
    end

    assert_redirected_to contract_subject_roles_path
  end
end
