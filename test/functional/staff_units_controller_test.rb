require 'test_helper'

class StaffUnitsControllerTest < ActionController::TestCase
  setup do
    @staff_unit = staff_units(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:staff_units)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create staff_unit" do
    assert_difference('StaffUnit.count') do
      post :create, staff_unit: { department_id: @staff_unit.department_id, descr: @staff_unit.descr, head: @staff_unit.head, id: @staff_unit.id, position_id: @staff_unit.position_id }
    end

    assert_redirected_to staff_unit_path(assigns(:staff_unit))
  end

  test "should show staff_unit" do
    get :show, id: @staff_unit
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @staff_unit
    assert_response :success
  end

  test "should update staff_unit" do
    put :update, id: @staff_unit, staff_unit: { department_id: @staff_unit.department_id, descr: @staff_unit.descr, head: @staff_unit.head, id: @staff_unit.id, position_id: @staff_unit.position_id }
    assert_redirected_to staff_unit_path(assigns(:staff_unit))
  end

  test "should destroy staff_unit" do
    assert_difference('StaffUnit.count', -1) do
      delete :destroy, id: @staff_unit
    end

    assert_redirected_to staff_units_path
  end
end
