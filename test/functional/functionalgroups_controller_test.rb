# -*- encoding : utf-8 -*-
require 'test_helper'

class FunctionalgroupsControllerTest < ActionController::TestCase
  setup do
    @functionalgroup = functionalgroups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:functionalgroups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create functionalgroup" do
    assert_difference('Functionalgroup.count') do
      post :create, functionalgroup: { descr: @functionalgroup.descr, lft: @functionalgroup.lft, link: @functionalgroup.link, name: @functionalgroup.name, parent_id: @functionalgroup.parent_id, part: @functionalgroup.part, rgt: @functionalgroup.rgt }
    end

    assert_redirected_to functionalgroup_path(assigns(:functionalgroup))
  end

  test "should show functionalgroup" do
    get :show, id: @functionalgroup
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @functionalgroup
    assert_response :success
  end

  test "should update functionalgroup" do
    put :update, id: @functionalgroup, functionalgroup: { descr: @functionalgroup.descr, lft: @functionalgroup.lft, link: @functionalgroup.link, name: @functionalgroup.name, parent_id: @functionalgroup.parent_id, part: @functionalgroup.part, rgt: @functionalgroup.rgt }
    assert_redirected_to functionalgroup_path(assigns(:functionalgroup))
  end

  test "should destroy functionalgroup" do
    assert_difference('Functionalgroup.count', -1) do
      delete :destroy, id: @functionalgroup
    end

    assert_redirected_to functionalgroups_path
  end
end
