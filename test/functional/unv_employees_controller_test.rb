require 'test_helper'

class UnvEmployeesControllerTest < ActionController::TestCase
  setup do
    @unv_employee = unv_employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unv_employees)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unv_employee" do
    assert_difference('UnvEmployee.count') do
      post :create, unv_employee: { actual_end: @unv_employee.actual_end, actual_start: @unv_employee.actual_start, employee_id: @unv_employee.employee_id, id: @unv_employee.id, planned_end: @unv_employee.planned_end, planned_start: @unv_employee.planned_start, unv_type_id: @unv_employee.unv_type_id }
    end

    assert_redirected_to unv_employee_path(assigns(:unv_employee))
  end

  test "should show unv_employee" do
    get :show, id: @unv_employee
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @unv_employee
    assert_response :success
  end

  test "should update unv_employee" do
    put :update, id: @unv_employee, unv_employee: { actual_end: @unv_employee.actual_end, actual_start: @unv_employee.actual_start, employee_id: @unv_employee.employee_id, id: @unv_employee.id, planned_end: @unv_employee.planned_end, planned_start: @unv_employee.planned_start, unv_type_id: @unv_employee.unv_type_id }
    assert_redirected_to unv_employee_path(assigns(:unv_employee))
  end

  test "should destroy unv_employee" do
    assert_difference('UnvEmployee.count', -1) do
      delete :destroy, id: @unv_employee
    end

    assert_redirected_to unv_employees_path
  end
end
