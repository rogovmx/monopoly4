# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportloadingsControllerTest < ActionController::TestCase
  setup do
    @transportloading = transportloadings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportloadings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportloading" do
    assert_difference('Transportloading.count') do
      post :create, transportloading: { descr: @transportloading.descr, id: @transportloading.id, loading: @transportloading.loading }
    end

    assert_redirected_to transportloading_path(assigns(:transportloading))
  end

  test "should show transportloading" do
    get :show, id: @transportloading
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportloading
    assert_response :success
  end

  test "should update transportloading" do
    put :update, id: @transportloading, transportloading: { descr: @transportloading.descr, id: @transportloading.id, loading: @transportloading.loading }
    assert_redirected_to transportloading_path(assigns(:transportloading))
  end

  test "should destroy transportloading" do
    assert_difference('Transportloading.count', -1) do
      delete :destroy, id: @transportloading
    end

    assert_redirected_to transportloadings_path
  end
end
