# -*- encoding : utf-8 -*-
require 'test_helper'

class TransportecologicalsControllerTest < ActionController::TestCase
  setup do
    @transportecological = transportecologicals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:transportecologicals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create transportecological" do
    assert_difference('Transportecological.count') do
      post :create, transportecological: { id: @transportecological.id, name: @transportecological.name }
    end

    assert_redirected_to transportecological_path(assigns(:transportecological))
  end

  test "should show transportecological" do
    get :show, id: @transportecological
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @transportecological
    assert_response :success
  end

  test "should update transportecological" do
    put :update, id: @transportecological, transportecological: { id: @transportecological.id, name: @transportecological.name }
    assert_redirected_to transportecological_path(assigns(:transportecological))
  end

  test "should destroy transportecological" do
    assert_difference('Transportecological.count', -1) do
      delete :destroy, id: @transportecological
    end

    assert_redirected_to transportecologicals_path
  end
end
