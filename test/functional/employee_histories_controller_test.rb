require 'test_helper'

class EmployeeHistoriesControllerTest < ActionController::TestCase
  setup do
    @employee_history = employee_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employee_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employee_history" do
    assert_difference('EmployeeHistory.count') do
      post :create, employee_history: { contractor_id: @employee_history.contractor_id, contractor_name: @employee_history.contractor_name, department_id: @employee_history.department_id, department_name: @employee_history.department_name, employee_id: @employee_history.employee_id, employment_date: @employee_history.employment_date, end_date: @employee_history.end_date, id: @employee_history.id, leaving_date: @employee_history.leaving_date, person_id: @employee_history.person_id, position_id: @employee_history.position_id, position_name: @employee_history.position_name, staff_unit_id: @employee_history.staff_unit_id, start_date: @employee_history.start_date }
    end

    assert_redirected_to employee_history_path(assigns(:employee_history))
  end

  test "should show employee_history" do
    get :show, id: @employee_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employee_history
    assert_response :success
  end

  test "should update employee_history" do
    put :update, id: @employee_history, employee_history: { contractor_id: @employee_history.contractor_id, contractor_name: @employee_history.contractor_name, department_id: @employee_history.department_id, department_name: @employee_history.department_name, employee_id: @employee_history.employee_id, employment_date: @employee_history.employment_date, end_date: @employee_history.end_date, id: @employee_history.id, leaving_date: @employee_history.leaving_date, person_id: @employee_history.person_id, position_id: @employee_history.position_id, position_name: @employee_history.position_name, staff_unit_id: @employee_history.staff_unit_id, start_date: @employee_history.start_date }
    assert_redirected_to employee_history_path(assigns(:employee_history))
  end

  test "should destroy employee_history" do
    assert_difference('EmployeeHistory.count', -1) do
      delete :destroy, id: @employee_history
    end

    assert_redirected_to employee_histories_path
  end
end
