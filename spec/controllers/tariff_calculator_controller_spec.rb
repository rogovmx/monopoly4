require 'rails_helper'

RSpec.describe TariffCalculatorController, :type => :controller do

  describe "GET market" do
    it "returns http success" do
      get :market
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET average" do
    it "returns http success" do
      get :average
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET zero" do
    it "returns http success" do
      get :zero
      expect(response).to have_http_status(:success)
    end
  end

end
