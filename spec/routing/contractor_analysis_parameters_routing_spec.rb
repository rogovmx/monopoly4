require "rails_helper"

RSpec.describe ContractorAnalysisParametersController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/contractor_analysis_parameters").to route_to("contractor_analysis_parameters#index")
    end

    it "routes to #new" do
      expect(:get => "/contractor_analysis_parameters/new").to route_to("contractor_analysis_parameters#new")
    end

    it "routes to #show" do
      expect(:get => "/contractor_analysis_parameters/1").to route_to("contractor_analysis_parameters#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/contractor_analysis_parameters/1/edit").to route_to("contractor_analysis_parameters#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/contractor_analysis_parameters").to route_to("contractor_analysis_parameters#create")
    end

    it "routes to #update" do
      expect(:put => "/contractor_analysis_parameters/1").to route_to("contractor_analysis_parameters#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/contractor_analysis_parameters/1").to route_to("contractor_analysis_parameters#destroy", :id => "1")
    end

  end
end
