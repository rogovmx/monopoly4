require "rails_helper"

RSpec.describe GpsNavTransportsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/gps_nav_transports").to route_to("gps_nav_transports#index")
    end

    it "routes to #new" do
      expect(:get => "/gps_nav_transports/new").to route_to("gps_nav_transports#new")
    end

    it "routes to #show" do
      expect(:get => "/gps_nav_transports/1").to route_to("gps_nav_transports#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/gps_nav_transports/1/edit").to route_to("gps_nav_transports#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/gps_nav_transports").to route_to("gps_nav_transports#create")
    end

    it "routes to #update" do
      expect(:put => "/gps_nav_transports/1").to route_to("gps_nav_transports#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/gps_nav_transports/1").to route_to("gps_nav_transports#destroy", :id => "1")
    end

  end
end
