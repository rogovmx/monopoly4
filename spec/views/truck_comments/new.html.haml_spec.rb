require 'rails_helper'

RSpec.describe "truck_comments/new", :type => :view do
  before(:each) do
    assign(:truck_comment, TruckComment.new(
      :truck_id => "MyString",
      :comment => "MyText",
      :user_id => "MyString",
      :is_close => false
    ))
  end

  it "renders new truck_comment form" do
    render

    assert_select "form[action=?][method=?]", truck_comments_path, "post" do

      assert_select "input#truck_comment_truck_id[name=?]", "truck_comment[truck_id]"

      assert_select "textarea#truck_comment_comment[name=?]", "truck_comment[comment]"

      assert_select "input#truck_comment_user_id[name=?]", "truck_comment[user_id]"

      assert_select "input#truck_comment_is_close[name=?]", "truck_comment[is_close]"
    end
  end
end
