require 'rails_helper'

RSpec.describe "transportmodel1cs/index", :type => :view do
  before(:each) do
    assign(:transportmodel1cs, [
      Transportmodel1c.create!(
        :transportmodel_1c_id => "Transportmodel 1c",
        :transportmodel_1c_name => "Transportmodel 1c Name",
        :transportmodel_id => "Transportmodel",
        :is_close => false
      ),
      Transportmodel1c.create!(
        :transportmodel_1c_id => "Transportmodel 1c",
        :transportmodel_1c_name => "Transportmodel 1c Name",
        :transportmodel_id => "Transportmodel",
        :is_close => false
      )
    ])
  end

  it "renders a list of transportmodel1cs" do
    render
    assert_select "tr>td", :text => "Transportmodel 1c".to_s, :count => 2
    assert_select "tr>td", :text => "Transportmodel 1c Name".to_s, :count => 2
    assert_select "tr>td", :text => "Transportmodel".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
