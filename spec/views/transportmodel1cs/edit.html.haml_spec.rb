require 'rails_helper'

RSpec.describe "transportmodel1cs/edit", :type => :view do
  before(:each) do
    @transportmodel1c = assign(:transportmodel1c, Transportmodel1c.create!(
      :transportmodel_1c_id => "MyString",
      :transportmodel_1c_name => "MyString",
      :transportmodel_id => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit transportmodel1c form" do
    render

    assert_select "form[action=?][method=?]", transportmodel1c_path(@transportmodel1c), "post" do

      assert_select "input#transportmodel1c_transportmodel_1c_id[name=?]", "transportmodel1c[transportmodel_1c_id]"

      assert_select "input#transportmodel1c_transportmodel_1c_name[name=?]", "transportmodel1c[transportmodel_1c_name]"

      assert_select "input#transportmodel1c_transportmodel_id[name=?]", "transportmodel1c[transportmodel_id]"

      assert_select "input#transportmodel1c_is_close[name=?]", "transportmodel1c[is_close]"
    end
  end
end
