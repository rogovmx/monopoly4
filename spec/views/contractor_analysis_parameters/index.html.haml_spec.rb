require 'rails_helper'

RSpec.describe "contractor_analysis_parameters/index", :type => :view do
  before(:each) do
    assign(:contractor_analysis_parameters, [
      ContractorAnalysisParameter.create!(
        :name => "Name"
      ),
      ContractorAnalysisParameter.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of contractor_analysis_parameters" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
