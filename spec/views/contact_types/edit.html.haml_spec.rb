require 'rails_helper'

RSpec.describe "contact_types/edit", :type => :view do
  before(:each) do
    @contact_type = assign(:contact_type, ContactType.create!(
      :name => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit contact_type form" do
    render

    assert_select "form[action=?][method=?]", contact_type_path(@contact_type), "post" do

      assert_select "input#contact_type_name[name=?]", "contact_type[name]"

      assert_select "input#contact_type_is_close[name=?]", "contact_type[is_close]"
    end
  end
end
