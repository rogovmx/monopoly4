require 'rails_helper'

RSpec.describe "booking_histories/edit", :type => :view do
  before(:each) do
    @booking_history = assign(:booking_history, BookingHistory.create!(
      :booking_id => "MyString",
      :status => false,
      :status_user_id => "MyString",
      :is_close => false
    ))
  end

  it "renders the edit booking_history form" do
    render

    assert_select "form[action=?][method=?]", booking_history_path(@booking_history), "post" do

      assert_select "input#booking_history_booking_id[name=?]", "booking_history[booking_id]"

      assert_select "input#booking_history_status[name=?]", "booking_history[status]"

      assert_select "input#booking_history_status_user_id[name=?]", "booking_history[status_user_id]"

      assert_select "input#booking_history_is_close[name=?]", "booking_history[is_close]"
    end
  end
end
