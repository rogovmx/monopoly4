require 'rails_helper'

RSpec.describe "extra_spendings/index", :type => :view do
  before(:each) do
    assign(:extra_spendings, [
      ExtraSpending.create!(
        :person_id => "Person",
        :transport_id => "Transport",
        :extra_spending_type_id => "Extra Spending Type",
        :spending_sum => "9.99",
        :is_urgently => false,
        :is_required_payment => false,
        :user_id => "User",
        :agreement_id => "Agreement",
        :agreement_result => 1,
        :agr_text => "MyText",
        :payment_request_id => "Payment Request",
        :is_request_status => false,
        :request_error => "MyText",
        :agreement_request_id => "Agreement Request",
        :request_result => 2,
        :agreed_sum => "9.99",
        :is_close => ""
      ),
      ExtraSpending.create!(
        :person_id => "Person",
        :transport_id => "Transport",
        :extra_spending_type_id => "Extra Spending Type",
        :spending_sum => "9.99",
        :is_urgently => false,
        :is_required_payment => false,
        :user_id => "User",
        :agreement_id => "Agreement",
        :agreement_result => 1,
        :agr_text => "MyText",
        :payment_request_id => "Payment Request",
        :is_request_status => false,
        :request_error => "MyText",
        :agreement_request_id => "Agreement Request",
        :request_result => 2,
        :agreed_sum => "9.99",
        :is_close => ""
      )
    ])
  end

  it "renders a list of extra_spendings" do
    render
    assert_select "tr>td", :text => "Person".to_s, :count => 2
    assert_select "tr>td", :text => "Transport".to_s, :count => 2
    assert_select "tr>td", :text => "Extra Spending Type".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "User".to_s, :count => 2
    assert_select "tr>td", :text => "Agreement".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Payment Request".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Agreement Request".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
