require 'rails_helper'

RSpec.describe "extra_spendings/show", :type => :view do
  before(:each) do
    @extra_spending = assign(:extra_spending, ExtraSpending.create!(
      :person_id => "Person",
      :transport_id => "Transport",
      :extra_spending_type_id => "Extra Spending Type",
      :spending_sum => "9.99",
      :is_urgently => false,
      :is_required_payment => false,
      :user_id => "User",
      :agreement_id => "Agreement",
      :agreement_result => 1,
      :agr_text => "MyText",
      :payment_request_id => "Payment Request",
      :is_request_status => false,
      :request_error => "MyText",
      :agreement_request_id => "Agreement Request",
      :request_result => 2,
      :agreed_sum => "9.99",
      :is_close => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Person/)
    expect(rendered).to match(/Transport/)
    expect(rendered).to match(/Extra Spending Type/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/User/)
    expect(rendered).to match(/Agreement/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Payment Request/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Agreement Request/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(//)
  end
end
