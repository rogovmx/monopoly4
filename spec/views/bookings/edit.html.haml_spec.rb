require 'rails_helper'

RSpec.describe "bookings/edit", :type => :view do
  before(:each) do
    @booking = assign(:booking, Booking.create!(
      :booking_type_id => "MyString",
      :truck_id => "MyString",
      :trailer_id => "MyString",
      :driver_id => "MyString",
      :user_id => "MyString",
      :comment => "MyText",
      :status => false,
      :status_user_id => "MyString",
      :source_id => "MyString",
      :target_id => "MyString"
    ))
  end

  it "renders the edit booking form" do
    render

    assert_select "form[action=?][method=?]", booking_path(@booking), "post" do

      assert_select "input#booking_booking_type_id[name=?]", "booking[booking_type_id]"

      assert_select "input#booking_truck_id[name=?]", "booking[truck_id]"

      assert_select "input#booking_trailer_id[name=?]", "booking[trailer_id]"

      assert_select "input#booking_driver_id[name=?]", "booking[driver_id]"

      assert_select "input#booking_user_id[name=?]", "booking[user_id]"

      assert_select "textarea#booking_comment[name=?]", "booking[comment]"

      assert_select "input#booking_status[name=?]", "booking[status]"

      assert_select "input#booking_status_user_id[name=?]", "booking[status_user_id]"

      assert_select "input#booking_source_id[name=?]", "booking[source_id]"

      assert_select "input#booking_target_id[name=?]", "booking[target_id]"
    end
  end
end
