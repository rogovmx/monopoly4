require 'rails_helper'

RSpec.describe "bookings/index", :type => :view do
  before(:each) do
    assign(:bookings, [
      Booking.create!(
        :booking_type_id => "Booking Type",
        :truck_id => "Truck",
        :trailer_id => "Trailer",
        :driver_id => "Driver",
        :user_id => "User",
        :comment => "MyText",
        :status => false,
        :status_user_id => "Status User",
        :source_id => "Source",
        :target_id => "Target"
      ),
      Booking.create!(
        :booking_type_id => "Booking Type",
        :truck_id => "Truck",
        :trailer_id => "Trailer",
        :driver_id => "Driver",
        :user_id => "User",
        :comment => "MyText",
        :status => false,
        :status_user_id => "Status User",
        :source_id => "Source",
        :target_id => "Target"
      )
    ])
  end

  it "renders a list of bookings" do
    render
    assert_select "tr>td", :text => "Booking Type".to_s, :count => 2
    assert_select "tr>td", :text => "Truck".to_s, :count => 2
    assert_select "tr>td", :text => "Trailer".to_s, :count => 2
    assert_select "tr>td", :text => "Driver".to_s, :count => 2
    assert_select "tr>td", :text => "User".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Status User".to_s, :count => 2
    assert_select "tr>td", :text => "Source".to_s, :count => 2
    assert_select "tr>td", :text => "Target".to_s, :count => 2
  end
end
