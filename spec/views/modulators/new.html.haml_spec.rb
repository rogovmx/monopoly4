require 'rails_helper'

RSpec.describe "modulators/new", :type => :view do
  before(:each) do
    assign(:modulator, Modulator.new(
      :contractor_id => "MyString",
      :traffic => "9.99",
      :is_close => false
    ))
  end

  it "renders new modulator form" do
    render

    assert_select "form[action=?][method=?]", modulators_path, "post" do

      assert_select "input#modulator_contractor_id[name=?]", "modulator[contractor_id]"

      assert_select "input#modulator_traffic[name=?]", "modulator[traffic]"

      assert_select "input#modulator_is_close[name=?]", "modulator[is_close]"
    end
  end
end
