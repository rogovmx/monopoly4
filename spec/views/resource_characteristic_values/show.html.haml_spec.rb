require 'rails_helper'

RSpec.describe "resource_characteristic_values/show", :type => :view do
  before(:each) do
    @resource_characteristic_value = assign(:resource_characteristic_value, ResourceCharacteristicValue.create!(
      :resource_characteristic_id => "Resource Characteristic",
      :truck_id => "Truck",
      :trailer_id => "Trailer",
      :driver_id => "Driver",
      :name => "Name",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Resource Characteristic/)
    expect(rendered).to match(/Truck/)
    expect(rendered).to match(/Trailer/)
    expect(rendered).to match(/Driver/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/false/)
  end
end
