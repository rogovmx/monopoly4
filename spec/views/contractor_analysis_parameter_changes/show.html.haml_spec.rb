require 'rails_helper'

RSpec.describe "contractor_analysis_parameter_changes/show", :type => :view do
  before(:each) do
    @contractor_analysis_parameter_change = assign(:contractor_analysis_parameter_change, ContractorAnalysisParameterChange.create!(
      :contractor_analysis_parameter_id => "Contractor Analysis Parameter",
      :parameter_value => "",
      :is_close => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Contractor Analysis Parameter/)
    expect(rendered).to match(//)
    expect(rendered).to match(/false/)
  end
end
