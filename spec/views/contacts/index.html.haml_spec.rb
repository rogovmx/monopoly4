require 'rails_helper'

RSpec.describe "contacts/index", :type => :view do
  before(:each) do
    assign(:contacts, [
      Contact.create!(
        :contact_type_id => "Contact Type",
        :person_id => "Person",
        :name => "Name",
        :is_close => false
      ),
      Contact.create!(
        :contact_type_id => "Contact Type",
        :person_id => "Person",
        :name => "Name",
        :is_close => false
      )
    ])
  end

  it "renders a list of contacts" do
    render
    assert_select "tr>td", :text => "Contact Type".to_s, :count => 2
    assert_select "tr>td", :text => "Person".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
