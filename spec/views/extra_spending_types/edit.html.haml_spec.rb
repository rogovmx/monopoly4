require 'rails_helper'

RSpec.describe "extra_spending_types/edit", :type => :view do
  before(:each) do
    @extra_spending_type = assign(:extra_spending_type, ExtraSpendingType.create!(
      :name => "MyString",
      :is_close => ""
    ))
  end

  it "renders the edit extra_spending_type form" do
    render

    assert_select "form[action=?][method=?]", extra_spending_type_path(@extra_spending_type), "post" do

      assert_select "input#extra_spending_type_name[name=?]", "extra_spending_type[name]"

      assert_select "input#extra_spending_type_is_close[name=?]", "extra_spending_type[is_close]"
    end
  end
end
