require 'rails_helper'

RSpec.describe "extra_spending_types/show", :type => :view do
  before(:each) do
    @extra_spending_type = assign(:extra_spending_type, ExtraSpendingType.create!(
      :name => "Name",
      :is_close => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(//)
  end
end
