require 'rails_helper'

RSpec.describe "extra_spending_types/index", :type => :view do
  before(:each) do
    assign(:extra_spending_types, [
      ExtraSpendingType.create!(
        :name => "Name",
        :is_close => ""
      ),
      ExtraSpendingType.create!(
        :name => "Name",
        :is_close => ""
      )
    ])
  end

  it "renders a list of extra_spending_types" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
