require 'rails_helper'

RSpec.describe "contractor_responsibles/index", :type => :view do
  before(:each) do
    assign(:contractor_responsibles, [
      ContractorResponsible.create!(
        :contractor_id => "Contractor",
        :employee_id => "Employee",
        :is_close => false
      ),
      ContractorResponsible.create!(
        :contractor_id => "Contractor",
        :employee_id => "Employee",
        :is_close => false
      )
    ])
  end

  it "renders a list of contractor_responsibles" do
    render
    assert_select "tr>td", :text => "Contractor".to_s, :count => 2
    assert_select "tr>td", :text => "Employee".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
