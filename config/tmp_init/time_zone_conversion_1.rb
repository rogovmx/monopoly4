module ActiveRecord
  module AttributeMethods
    module TimeZoneConversion

      module ClassMethods
        private
        def create_time_zone_conversion_attribute?(name, column)
          time_zone_aware_attributes && !self.skip_time_zone_conversion_for_attributes.include?(name.to_sym) 
        end
      end
    end
  end
end
