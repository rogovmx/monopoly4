module ActiveRecord
  module AttributeMethods
    module TimeZoneConversion
      module ClassMethods
        protected
        # Defined for all +datetime+ and +timestamp+ attributes when +time_zone_aware_attributes+ are enabled.
        # This enhanced write method will automatically convert the time passed to it to the zone stored in Time.zone.
        def define_method_attribute=(attr_name)
            super
        end

      end
    end
  end
end
