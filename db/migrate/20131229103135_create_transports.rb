# -*- encoding : utf-8 -*-
class CreateTransports < ActiveRecord::Migration
  def change
    create_table :transports, id: false do |t|
      t.uuid :id, null: false, :primary_key => true
      t.string :name
      t.text :description
      t.string :vin
      t.string :reg_num
      t.string :body_num
      t.string :chassis_num
      t.string :engine_num
      t.integer :year, precision: 4
      t.numeric :weight, precision: 16, scale: 2
      t.numeric :capacity, precision: 16, scale: 2
      t.numeric :overall_height, precision: 16, scale: 2
      t.numeric :overall_width, precision: 16, scale: 2
      t.numeric :overall_length, precision: 16, scale: 2
      t.numeric :useful_height, precision: 16, scale: 2
      t.numeric :useful_width, precision: 16, scale: 2
      t.numeric :useful_length, precision: 16, scale: 2
      t.numeric :useful_volume, precision: 16, scale: 2
      t.date :registration_date
      t.date :deregistration_date     
      t.boolean :is_close, default: false
      t.timestamps
    end
  end
end
