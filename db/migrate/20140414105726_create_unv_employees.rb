class CreateUnvEmployees < ActiveRecord::Migration
  def change
    create_table :unv_employees, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :employee_id
      t.uuid :unv_type_id
      t.datetime :planned_start
      t.datetime :planned_end
      t.datetime :actual_start
      t.datetime :actual_end

      t.timestamps
    end
  end
end
