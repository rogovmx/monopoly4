# -*- encoding : utf-8 -*-
class CreateEnginemodelsTransports < ActiveRecord::Migration
  def change
    create_table :enginemodels_transports, id: false do |t|
      t.uuid :enginemodel_id
      t.uuid :transport_id
    end
  end
end
