# -*- encoding : utf-8 -*-
class CreateColumns < ActiveRecord::Migration
  def change
    create_table :columns, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :table_id
      t.string :name
      t.text :description
      t.date :start
      t.date :end
      t.boolean :is_close

      t.timestamps
    end
  end
end
