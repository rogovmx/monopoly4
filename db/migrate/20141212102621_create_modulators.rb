class CreateModulators < ActiveRecord::Migration
  def change
    create_table :modulators do |t|
      t.string :contractor_id
      t.decimal :traffic
      t.boolean :is_close

      t.timestamps
    end
  end
end
