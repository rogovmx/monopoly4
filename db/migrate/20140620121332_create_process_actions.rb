class CreateProcessActions < ActiveRecord::Migration
  def change
    create_table :process_actions do |t|
      t.string :name
      t.text :descr
      t.boolean :is_close

      t.timestamps
    end
  end
end
