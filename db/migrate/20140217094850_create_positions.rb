# -*- encoding : utf-8 -*-
class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.string :name
      t.text :descr
      t.boolean :is_close
      t.timestamps
    end
  end
end
