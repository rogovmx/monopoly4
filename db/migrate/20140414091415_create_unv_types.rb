class CreateUnvTypes < ActiveRecord::Migration
  def change
    create_table :unv_types, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :resourse_type_id
      t.string :name
      t.text :descr
      t.boolean :is_close
      t.timestamps
    end
  end
end
