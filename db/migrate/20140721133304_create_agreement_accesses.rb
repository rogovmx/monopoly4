class CreateAgreementAccesses < ActiveRecord::Migration
  def change
    create_table :agreement_accesses do |t|
      t.string :agreement_id
      t.string :staff_unit_id
      t.boolean :is_close

      t.timestamps
    end
  end
end
