# -*- encoding : utf-8 -*-
class CreateTransportTransportmodel < ActiveRecord::Migration
  def change
    create_table :transportmodels_transports, id: false do |t|
      t.uuid :transport_id
      t.uuid :transportmodel_id
    end
  end
end
