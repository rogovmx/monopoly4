class CreateContractRelationships < ActiveRecord::Migration
  def change
    create_table :contract_relationships, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :contract_id
      t.uuid :contract_kind_role_id
      t.uuid :contractor_id
      t.boolean :is_close

      t.timestamps
    end
  end
end
