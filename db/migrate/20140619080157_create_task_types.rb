class CreateTaskTypes < ActiveRecord::Migration
  def change
    create_table :task_types do |t|
      t.string :task_type
      t.text :descr

      t.timestamps
    end
  end
end
