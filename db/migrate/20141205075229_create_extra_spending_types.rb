class CreateExtraSpendingTypes < ActiveRecord::Migration
  def change
    create_table :extra_spending_types do |t|
      t.string :name
      t.boolean :is_close

      t.timestamps
    end
  end
end
