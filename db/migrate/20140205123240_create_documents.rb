# -*- encoding : utf-8 -*-
class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :parent_id
      t.uuid :documentkind_id
      t.uuid :contract_id
      t.uuid :contractor_id
      t.string :series
      t.string :number
      t.string :category
      t.string :registration
      t.string :issued_by
      t.date :issued_date
      t.date :start_date
      t.date :end_date
      t.boolean :is_close, default: false
      t.integer :lft
      t.integer :rgt
      t.string :type
      t.timestamps
    end
  end
end
