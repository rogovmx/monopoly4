class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :person_id
      t.uuid :staff_unit_id
      t.date :employment_date
      t.date :leaving_date
      t.date :start_date
      t.date :end_date
      t.boolean :is_close
      t.timestamps
    end
  end
end
