class CreateTruckDrivers < ActiveRecord::Migration
  def change
    create_table :truck_drivers, id: false do |t|
      t.uuid :id, null: false, primary_key: true
      t.uuid :transport_id
      t.uuid :employee_id
      t.boolean :is_close
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
