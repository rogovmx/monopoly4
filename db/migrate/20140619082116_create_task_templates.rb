class CreateTaskTemplates < ActiveRecord::Migration
  def change
    create_table :task_templates do |t|
      t.string :task_type_id
      t.string :name
      t.text :descr
      t.string :objective
      t.integer :normative_runtime
      t.integer :prioryty

      t.timestamps
    end
  end
end
