class CreateExtraSpendings < ActiveRecord::Migration
  def change
    create_table :extra_spendings do |t|
      t.string :person_id
      t.string :transport_id
      t.string :extra_spending_type_id
      t.decimal :spending_sum
      t.boolean :is_urgently
      t.boolean :is_required_payment
      t.text :descr
      t.string :user_id
      t.string :agreement_id
      t.integer :agreement_result
      t.text :agr_text
      t.datetime :agr_date
      t.string :payment_request_id
      t.boolean :is_request_status
      t.text :request_error
      t.string :agreement_request_id
      t.integer :request_result
      t.datetime :request_date
      t.decimal :agreed_sum
      t.boolean :is_close

      t.timestamps
    end
  end
end
