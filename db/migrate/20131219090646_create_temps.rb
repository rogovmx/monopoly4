# -*- encoding : utf-8 -*-
class CreateTemps < ActiveRecord::Migration
  def change
    create_table :temps do |t|
      t.integer :num1
      t.string :str1

      t.timestamps
    end
  end
end
