namespace :sms do

  task :send => :environment do
    require 'digest/md5'
    require 'net/http'
    
    i = 0
    end_time = Time.now + 2.hours
    @sended = 0
    
    begin
      
      break if Time.now > end_time
      i += 1
      puts "loop: #{i}"
      @sms = Sms.not_sent
      @is_ok = SmsResult.first.isOk
      if @is_ok and (@sms.size > 0)
      @sended = send_all_sms @sms
      puts "SMS size #{@sms.size}"  
      else
        @text = "SMS for send: #{@sms.size}. Flag_isOk: #{@is_ok}. Next send at: #{Time.now + 15.minutes}."
        @number = "79219512123"
        send_sms @number, @text
        @number = "79817199030"
        send_sms @number, @text
        puts @text
        sleep(900)
      end  
    end until @is_ok && (@sms.size > 1)
    
    @text = "#{@sended} СМС отправлено.  "
    @number = "79219512123"
    send_sms @number, @text
    @number = "79817199030"
    send_sms @number, @text
    @number = "79118192010"
    send_sms @number, @text
    puts "SMS size #{@sms.size}"
    puts "IsOk #{@is_ok}"
    puts Rails.env
    puts @text
  end
  
  def send_all_sms(sms_for_send)
    @snd = 0
    sms_for_send.each do |sms|
      
      File.open('log/report.sms', 'w'){ |file| file.write sms.sms_text; file.write "\n" }

      sleep(0.1)
      @number = sms.phone_number
      @text = sms.sms_text      

      content = send_sms(@number, @text).body
      pos1 = content.index('</long>')
      num = content[pos1-8..pos1-1] if pos1
      sms.message_id = num
      sms.save
      puts @text
      @snd += 1
    end
    @snd
  end
  
  def send_sms(number, text)
    @pass = Digest::MD5.hexdigest('79119887257').to_s.strip
    url = "http://mcommunicator.ru/m2m/m2m_api.asmx/SendMessage"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    parameters =  {"msid" => "#{number}", "message" => "#{text}", "naming" => "79857707575", "login" => "79119887257", "password" => "#{@pass}"}
    request.set_form_data(parameters)
    @response = http.request(request)
  end
  
  def send_sms_test
   parameters =  {"msid" => "#{@number}", "message" => "#{@text}", "naming" => "79857707575", "time" => Time.now}
   File.open('log/test.sms', 'w'){ |file| file.write parameters; file.write "\n" }
  end
  
end


#  END namespace :sms ---------------------------------------------------------------------------------------------------------------------------------------------

namespace :wialon do

  task :test => :environment do
    require 'net/http'
    require 'json'
    require 'date'

    time = Time.now
    puts "Wialon test start, #{time.to_s}"

    s_date = 1393279200
    e_date = 1393568760

    10.times do |i|
      resp = wialon_request_test 2, 3573842, s_date - i*1000, e_date + i*100
      #File.open('log/report.wialon', 'a'){ |file| file.write "#{i}"; file.write resp; file.write "\n" }
      puts "#{i}:  #{resp}"
    end

    puts "Wialon test Ok, #{Time.now} - #{time}.to_s}"

  end


  task :can_report => :environment do
    require 'net/http'
    require 'json'
    require 'date'

    puts "Wialon start, #{Time.now.to_s}"

    resp = wialon_request('core/login',"{'user':'monopoly','password':'123456'}")
    @eid = resp['eid']

    yd = 1.day.ago

    s_date = DateTime.new(yd.year, yd.month, yd.day, 0, 0, 0).to_time.to_i
    e_date = DateTime.new(yd.year, yd.month, yd.day, 23, 59, 59).to_time.to_i


    core = 'core/search_items'
    par = '{"spec":{"itemsType":"avl_unit","propName":"sys_name","propValueMask":"*","sortType":""},"force":1,"flags":1,"from":0,"to":0}'

    resp = wialon_request core, par, @eid


    @ids = []
    resp["items"].each{|i| @ids << i["id"]}

    core = 'report/exec_report'

    #шаблон отчета 2
    @ids.each do |id|
      par = "{'reportResourceId':1222731,'reportTemplateId':2,'reportObjectId':#{id},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"
      resp = wialon_request core, par, @eid
      Wialon.create({rec_data: resp.to_json, template: 2})
      #File.open('report.js', 'a'){ |file| file.write resp }
    end

    #шаблон отчета 1
    @ids.each do |id|
      par = "{'reportResourceId':1222731,'reportTemplateId':1,'reportObjectId':#{id},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"
      resp = wialon_request core, par, @eid
      Wialon.create({rec_data: resp, template: 1})
    end

    #шаблон отчета 8
    @ids.each do |id|
      par = "{'reportResourceId':1222731,'reportTemplateId':8,'reportObjectId':#{id},'reportObjectSecId':1,'interval':{'from':#{s_date},'to':#{e_date},'flags':0}}"
      resp = wialon_request core, par, @eid
      Wialon.create({rec_data: resp, template: 8})
    end


    puts "Wialon Ok, #{Time.now.to_s}"

  end

end


def wialon_request svc, params=nil, sid=nil
  http = Net::HTTP.new("hst-api.wialon.com", 443)

  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE

  request = Net::HTTP::Post.new("/wialon/ajax.html?svc=#{svc}#{params ? '&params='+params : nil}&sid=#{sid}")
  #http.set_debug_output($stdout)

  request["Content-Type"] = "application/x-www-form-urlencoded"

  response = http.request(request)
  JSON.parse(response.body)
  #response.body
end


def wialon_request_test reportTemplateId, reportObjectId, from, to

  http = Net::HTTP.new("192.168.0.15", 80)

  request = Net::HTTP::Get.new("/wialon?reportTemplateId=#{reportTemplateId}&reportObjectId=#{reportObjectId}&from=#{from}&to=#{to}")
  #http.set_debug_output($stdout)
  response = http.request(request)
  response.body
end