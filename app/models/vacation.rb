class Vacation < ActiveRecord::Base
  
  belongs_to :people_contractor, class_name: "PeopleContractor", foreign_key: :employee_id , primary_key: :employeeId
  has_one :person, through: :people_contractor
  
  default_scope lambda { visible }
  scope :visible, lambda { where("vacations.is_close != 1") }
  scope :drivers, lambda { joins(person: {employees: {staff_unit: :position}}).where("positions.name = ?", "Водитель") }
  
  
  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
  
end
