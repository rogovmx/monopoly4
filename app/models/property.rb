class Property < Monopolydata
  
  scope :visible, lambda { where("[properties].[is_close] != ?", true) }
  
  belongs_to :system
  belongs_to :objecttype
  
  validates :system_id, :objecttype_id, presence: true


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
