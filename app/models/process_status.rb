class ProcessStatus < ActiveRecord::Base

  scope :visible, lambda { where("[process_statuses].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
