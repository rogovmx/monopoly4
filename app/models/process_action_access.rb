class ProcessActionAccess < ActiveRecord::Base

  belongs_to :process_action
  belongs_to :process_template
  belongs_to :employee
  belongs_to :staff_unit


  scope :visible,  lambda { where("[process_action_accesses].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
