# -*- encoding : utf-8 -*-
class Phoneuse < ActiveRecord::Base
#  attr_accessible :is_close, :is_corporate, :issue_date, :person_id, :phone_id, :take_date, :id


  belongs_to :person
  belongs_to :phone

  before_validation :ensure_uuid

  validates :issue_date, :person_id, :phone_id, presence: true

  #audited

  scope :visible, lambda { where("is_close != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def ensure_uuid; self.id ||= SecureRandom.uuid end

end
