class ResourceCharacteristicValue < ActiveRecord::Base
  
  scope :visible, lambda { where("[is_close] != ?", true) }
  
  belongs_to :resource_characteristic
  
  validates :resource_characteristic_id, presence: true

  def self.res_char(char)
    ResourceCharacteristic.where(short_name: char).limit(1).first || false
  end
  
  def self.find_char_values(truck_id, trailer_id, driver_id)
    @trk, @trl, @drv = !truck_id.blank?, !trailer_id.blank?, !driver_id.blank?
    @out = {}
    @char_vals = 
      if (@trk && @trl && @drv)
        ResourceCharacteristicValue.where("truck_id = ? OR trailer_id = ? OR driver_id = ?", truck_id, trailer_id, driver_id).select(:resource_characteristic_id, :name)
      elsif (@trk && @trl) 
        ResourceCharacteristicValue.where("truck_id = ? OR trailer_id = ?", truck_id.downcase, trailer_id).select(:resource_characteristic_id, :name)
      elsif (@trl && @drv)
        ResourceCharacteristicValue.where("trailer_id = ? OR driver_id = ?", trailer_id.downcase, driver_id).select(:resource_characteristic_id, :name)
      elsif (@trk && @drv)
        ResourceCharacteristicValue.where("truck_id = ? OR driver_id = ?", truck_id.downcase, driver_id).select(:resource_characteristic_id, :name)
      elsif @trk 
        ResourceCharacteristicValue.where("truck_id = ?", truck_id).limit(1).select(:resource_characteristic_id, :name)
      elsif @drv
        ResourceCharacteristicValue.where("driver_id = ?", driver_id).limit(1).select(:resource_characteristic_id, :name)
      elsif @trl
        ResourceCharacteristicValue.where("trailer_id = ?", trailer_id).limit(1).select(:resource_characteristic_id, :name)
      else
        nil
      end
    @char_vals.each do |char|
      @out[char.resource_characteristic.short_name] = char.name
    end if @char_vals
    @out
  end
    
 
  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
end
