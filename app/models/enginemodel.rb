# -*- encoding : utf-8 -*-
class Enginemodel < ActiveRecord::Base
#  attr_accessible :displacement, :enginetype_id, :name, :power, :power_kvt, :is_close

 

  validates :name, :displacement, :power, :power_kvt, :enginetype_id,  presence: true
  validates  :displacement, :power, :power_kvt,  numericality: true

  has_many :transportmodels
  has_and_belongs_to_many :transports
  belongs_to :enginetype
  #audited

  scope :visible, lambda { where("is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  
  
end
