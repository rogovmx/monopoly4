class Transportmodel < ActiveRecord::Base
#  attr_accessible :name, :transportbrand_id, :weight, :capacity, :is_engine, :is_loading, :is_fuelling, :id, :is_close,
#                  :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume
# 
  before_validation :ensure_uuid

  validates :name, :transportbrand_id, :weight, :capacity, presence: true
  validates :weight, :capacity,  numericality: true
  validates :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume,  numericality: true, allow_blank: true
    

  #validate :has_transportkind, on: :update
  #validate :engine_loading_tank, on: :update

  default_scope lambda { visible.order(:transportbrand_id)}
  scope :visible, lambda { where("transportmodels.is_close != ?", true) }
  scope :trucks, lambda { where(transportkind_id: '27f61ba9-8d2d-489c-b890-f16423b4b935')}
  scope :trailers, lambda { where.not(transportkind_id: '27f61ba9-8d2d-489c-b890-f16423b4b935')}

  belongs_to :transportkind
  belongs_to :transportbrand
  belongs_to :enginemodel

  has_many :transports
  has_many :transporttanks, -> { order(:v_tank)}
  has_many :transportmodel1cs
  has_and_belongs_to_many :transportloadings
  #has_and_belongs_to_many :enginetypes
  #audited

  def has_transportkind
    errors.add(:transportkind, "Не указаны категория, тип и вид ТС") unless self.transportkind_id
  end

  def engine_loading_tank
    errors.add(:transportloading, "Не указан тип погрузки") if self.is_loading and self.transportloadings.blank?
    errors.add(:transporttank, "Не указан топливный бак") if self.is_fuelling and self.transporttanks.blank?
    errors.add(:enginetype, "Не указан тип двигателя") if self.is_engine and self.enginemodel.blank?
  end

  def kind_type_category
    "#{transportkind.transporttype.transportcategory.name if transportkind && transportkind.transporttype && transportkind.transporttype.transportcategory}. #{transportkind.transporttype.name if transportkind && transportkind.transporttype}. #{transportkind.name if transportkind}."
  end
  

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  
private

  
 def ensure_uuid
    self.id ||= SecureRandom.uuid
 end



end
