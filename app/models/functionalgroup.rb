class Functionalgroup < ActiveRecord::Base

  #attr_accessible :descr, :link, :name, :part, :is_close , :rgt, :lft, :parent_id
 


  
  has_and_belongs_to_many :users

  acts_as_nested_set
  #accepts_nested_attributes_for :functionalgroup
  #audited

  scope :visible, lambda { where("functionalgroups.is_close != ?", true) }

  #before_validation :delete_empty_parameters
  #before_create :ensure_uuid

  def ensure_uuid; self.id ||= SecureRandom.uuid end
  
  
  def descendents
    children.map do |child|
      [child] + child.descendents
    end.flatten.compact + [self]
  end

  

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  private

#  def delete_empty_parameters
#     Department.accessible_attributes.each do |c|
#       self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
#     end
#  end


end
