class ContractorResponsible < ActiveRecord::Base
  
  
  validates :contractor_id, :employee_id, :start_date, presence: true

  acts_as_nested_set

  belongs_to :contractor
  belongs_to :employee

  scope :visible, lambda { where("contractor_responsibles.is_close != ?", true).joins(:contractor).order('contractor_responsibles.created_at') }
  scope :chained, lambda {visible.where("[contractor_responsibles].[end_date] > ? or [contractor_responsibles].[end_date] IS NULL", Time.now)}
#  scope :active_chained, -> {chained.map(&:active_descendents).flatten.compact.uniq}

  def descendents
    chained_children_by_contractor.map do |child|
      [child] + child.descendents if child.descendents
    end.flatten.compact #if children_by_contractor
  end

  def active_descendents
    if  descendents
      (descendents + [self]).find_all { |d| d.leaf_by_contractor? }.uniq
    end
  end
  
  
  def chained?
    end_date.nil? or end_date > Time.now 
  end
  
  def leaf_by_contractor?
    leaf? || chained_children_by_contractor.blank?
  end

  
  def chained_children_by_contractor
    children.where(contractor_id: contractor_id).chained
  end
  
  def parent_by_contractor
    parent if contractor_id == parent.contractor_id if parent
  end
  
  
  def person_fullname
    employee.person_fullname if employee and employee.person
  end
  
  def person_info
    employee.person_info if employee and employee.person
  end

  def position
    employee.staff_unit.position.name if employee and employee.staff_unit and employee.staff_unit.position
  end
  
  def unchain
    self.update_attribute(:end_date, Time.now )
  end

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
