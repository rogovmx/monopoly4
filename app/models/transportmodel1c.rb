class Transportmodel1c < ActiveRecord::Base
  
  belongs_to :transportmodel
  
  scope :visible, lambda { where("is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
