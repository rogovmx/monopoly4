# -*- encoding : utf-8 -*-
class Testguid < ActiveRecord::Base
  #include ActiveUUID::UUID
  #attr_accessible :str

  #set_primary_key :id
  #before_create :set_guid_column
  #before_save :set_guid_column


  protected

  def set_guid_column
    self[:id] = SecureRandom.uuid
  end
end
