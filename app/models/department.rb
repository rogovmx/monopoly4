# -*- encoding : utf-8 -*-
class Department < ActiveRecord::Base
  #  attr_accessible :contractor_id, :descr, :id,  :name, :parent_id, :is_close

  acts_as_nested_set

  belongs_to :contractor
  has_many :staff_units

  #audited

  before_validation :ensure_uuid #, :delete_empty_parameters

  validates :contractor_id, :descr, :name, presence: true

  scope :visible, lambda { where("departments.is_close != ?", true).joins(:contractor).order('contractors.name') }
  scope :with_free_staff_units, lambda { visible.find_all{|d| d.staff_units.size > d.employees_sum} }


  def employees_sum
    self.staff_units.inject(0){ |r, s| r += s.employees.any? ? s.employees.size : 0 }
  end

  def employees
    emp = []
    self.staff_units.each{ |s| emp += s.employees }.compact
    emp
  end


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  def contractor_name
    self.contractor ? "Контрагент: #{self.contractor.name}, Подразделение: #{name}" : ''
  end


  private


  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
    Department.accessible_attributes.each do |c|
      self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
    end
  end

end
