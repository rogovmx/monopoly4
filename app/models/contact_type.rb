class ContactType < ActiveRecord::Base
  
  validates :name, presence: true


  has_many :contacts

  default_scope { order('name') } 
  scope :visible, lambda { where("is_close != ?", true)}


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
