class AgreementRequest < ActiveRecord::Base

  belongs_to :agreement
  has_many :question_answers, -> { order(question_date: :desc) }
  has_many :question_answers_no, -> {visible.where(answer_variant: 'No').order(question_date: :desc).includes(:person)}, :class_name => 'QuestionAnswer'
  has_many :question_answers_yes, -> {visible.where(answer_variant: 'Yes').order(question_date: :desc).includes(:person)}, :class_name => 'QuestionAnswer'

  #scope :visible, lambda {where("[agreement_requests].[is_close] != ?", true)}
  scope :find_by_transport, ->(tr_key) { where("agreement_requests.created_at > ? and initiator_object_id = ?", Time.now - 2.day, tr_key).includes(:question_answers).where.not(question_answers: {answer_variant: 'NaN'}).order("agreement_requests.created_at DESC") }
  scope :find_by_transport_and_time, ->(tr_key, time) { where("agreement_requests.created_at > ? and initiator_object_id = ?", Time.now - time.hours, tr_key).includes(:question_answers).where.not(question_answers: {answer_variant: 'NaN'}).order("agreement_requests.created_at DESC") }
  

  validates :staff_unit_id, :agreement_id, presence: true



  def persons_yes
    question_answers_yes.map { |q| "#{q.person.fullname}: [#{q.answer_text.blank? ? '-' : q.answer_text}]" if q.person }.compact.join("; ")
  end

  def persons_no
    question_answers_no.map { |q| "#{q.person.fullname}: [#{q.answer_text.blank? ? '-' : q.answer_text}]" if q.person }.compact.join("; ")
  end

  def persons_nan
    question_answers.where(answer_variant: 'NaN').includes(:person).map { |q| q.person.fullname if q.person }.compact.join("; ")
  end

end
