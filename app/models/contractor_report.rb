class ContractorReport < Monosql

  def self.contractor_list(from, to, contractor = 'NULL', direction = 'NULL')
    contr = contractor ?  "'#{contractor}'" : 'NULL'
    dir = direction ? "'#{direction}'" : 'NULL' 
    @out = []
    @data =  connection.select_all("exec [MonopolySun].[dbo].[ms_ContragentAnalitics_ContragentList] @MomentFrom=#{from}, @MomentTo=#{to}, @ContragentKey=#{contr}, @Except=0, @Direction=#{dir} ").to_a
    @data.each do |d|
      tmp = ContractorList.new
      tmp.key = d["ContragentKey"]
      tmp.name = d["FullName"]
      tmp.inn = d["INN"]
      tmp.kpp = d["KPP"]
      @out << tmp
    end
    @out
  end

  def self.direction_list(from, to, contractor = 'NULL', direction = 'NULL')
    contr = contractor ?  "'#{contractor}'" : 'NULL'
    dir = direction ? "'#{direction}'" : 'NULL' 
    @out = []
    @data =  connection.select_all("exec [MonopolySun].[dbo].[ms_ContragentAnalitics_DirectionList] @MomentFrom=#{from}, @MomentTo=#{to}, @ContragentKey=#{contr}, @Direction=#{dir} ").to_a
    @data.each do |d|
      tmp = DirectionList.new
      tmp.direction = d["Direction"]
      @out << tmp
    end
    @out
  end

  def self.report(from, to, contractor = 'NULL', direction = 'NULL')
    contr = contractor ?  "'#{contractor}'" : 'NULL'
    dir = direction ? "'#{direction}'" : 'NULL' 
    @out = []
    @data =  connection.select_all("exec [MonopolySun].[dbo].[ms_ContragentAnalitics_ContractList] @MomentFrom=#{from}, @MomentTo=#{to}, @ContragentKey=#{contr}, @Direction=#{dir}").to_a
    @data.each do |d|
      tmp = Report.new
      tmp.contractor = d["Контрагент"]
      tmp.contract = d["Договор"]
      tmp.direction = d["Направление"]
      tmp.tariff = d["Тариф"]
      tmp.struct_lising_cost = d["Стоимость структуры и лизинга"]
      tmp.variable_cost = d["Переменные издержки"]
      tmp.account_receivable_cost = d["Стоимость дебиторки"]
      tmp.total = d["Итого"]
      tmp.quantity = d["Кол-во заявок"]
      tmp.average = d["Среднее"]
      tmp.contractor_id = d["ContragentKey"]
      @out << tmp
    end
    @out
  end

  def self.report_info(from, to, contractor = 'NULL', direction = 'NULL', r_type=1, except=1)
    contr = contractor ?  "'#{contractor}'" : 'NULL'
    dir = direction ? "'#{direction}'" : 'NULL' 
    if r_type.to_i == 4
      @except = 0 
    else
      @except = 1
    end  
    @out = []
    @data =  connection.select_all("exec [MonopolySun].[dbo].[ms_ContragentAnalitics_ContragentList] @MomentFrom=#{from}, @MomentTo=#{to}, @ContragentKey=#{contr}, @Direction=#{dir}, @Except=#{@except}, @ReportType=#{r_type}").to_a
    @data.each do |d|
      tmp = Report.new
      tmp.contractor = d["Контрагент"]
      tmp.contract = d["Договор"]
      tmp.direction = d["Направление"]
      tmp.tariff = d["Тариф"]
      tmp.struct_lising_cost = d["Стоимость структуры и лизинга"]
      tmp.variable_cost = d["Переменные издержки"]
      tmp.account_receivable_cost = d["Стоимость дебиторки"]
      tmp.total = d["Итого"]
      tmp.quantity = d["Кол-во заявок"]
      tmp.average = d["Среднее"]
      tmp.contractor_id = d["ContragentKey"]
      tmp.number = d["№ заявки"] if d["№ заявки"]
      @out << tmp
    end
    @out
  end
 
end

class ContractorList < Object
  attr_accessor :key, :name, :inn, :kpp
end  

class DirectionList < Object
  attr_accessor :direction
end  

class Report < Object
  attr_accessor :contractor, :contract, :direction, :tariff, :struct_lising_cost, :variable_cost, :account_receivable_cost, :total, :quantity, :average, :contractor_id, :number
end  

