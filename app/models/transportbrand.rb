# -*- encoding : utf-8 -*-
class Transportbrand < ActiveRecord::Base
#  attr_accessible  :name, :is_close

  #audited

  has_many :transportmodels, -> {order(:name)}

  validates :name, presence: true

  default_scope lambda {visible.order(:name)}
  scope :visible, lambda { where("is_close != ?", true) }

  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
