class ProcessType < ActiveRecord::Base

  has_many :process_templates

  scope :visible, lambda { where("[process_types].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

end
