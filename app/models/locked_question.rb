class LockedQuestion < ActiveRecord::Base
  
  def self.get_lock_time
    url = "http://esbsrv01.monopoly.su/esb/number.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    response.body
  end
  
  def self.locked_q?(id)
    id.blank? ? nil : LockedQuestion.where("created_at > ?", LockedQuestion.get_lock_time.to_i.minutes.ago).where(question_id: id).limit(1).first
#    id.blank? ? nil : LockedQuestion.where("created_at > ?", (3.hours + 1.minutes).ago).where(question_id: id).limit(1).first.try(:user_id)
  rescue 
    nil
  end
  
end
