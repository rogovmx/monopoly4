class Contact < ActiveRecord::Base
  
    
  validates :name, :contact_type_id, :person_id, presence: true


  belongs_to :contact_type
  belongs_to :person

  scope :visible, lambda { where("is_close != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  
end
