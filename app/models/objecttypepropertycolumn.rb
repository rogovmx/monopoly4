# -*- encoding : utf-8 -*-
class Objecttypepropertycolumn < Monopolydata
#  attr_accessible :column_id, :end, :id, :is_close, :objecttypeproperty_id, :start

  belongs_to :objecttypeproperty
  belongs_to :column

  scope :visible, lambda { where("[objecttypepropertycolumns].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end
  


end
