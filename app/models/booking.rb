require 'unv'

class Booking < ActiveRecord::Base
  
  FILTER_LOGICAL_OPS = ['ИЛИ', 'И', 'И НЕ']  
  COLUMN_NAMES = ['№ машины', 'Логист', 'П', 'ДР', 'Д', 'Р', 'ПВ', 'Регион высв', 'Город высв', '№ заявки', 
                'Заказчик по текущей заявке', 'Маршрут по текущей заявке', 'Статус текущей заявки', '№', 'Комментарий логиста', 
                'Дата высв', 'Время высв', 'Последний комментарий координатора', '№ след заявки', 'Менеджер',
                'Заказчик по следующей заявке', 'Регион погрузки следующей заявки', 'Маршрут по следующей заявке', 'Дата погр', 'Время погр', 'Тариф']
#  COLUMN_NAMES = ['№ машины', 'Логист', 'П', 'ПН', 'ДР', 'Д', 'Р', 'ПВ', 'Регион высв', 'Город высв', '№ заявки', 
#                'Заказчик по текущей заявке', 'Маршрут по текущей заявке', 'Статус текущей заявки', '№', 'Комментарий логиста', 
#                'Дата высв', 'Время высв', 'Последний комментарий координатора', '№ след заявки', 'Менеджер',
#                'Заказчик по следующей заявке', 'Регион погрузки следующей заявки', 'Маршрут по следующей заявке', 'Дата погр', 'Время погр', 'Тариф']
  COLUMN_KEYS = ["truck_num", "logist_name", "p", "dr", "d", "r", "pv", "region_release", "city_release", "entry_num", 
                "contractor_previous_entry", "current_route_entry", "current_status", "current_operation", "logist_comment", 
                "release_date", "release_time", "coord_last_comment", "next_entry_num", "manager_name", "contractor_next_entry", 
                "region_loading", "next_route_entry", "loading_date", "loading_time", "tariff", "mileage", "rr_km", "truck_key", "trailer_key", 
                "driver_key", "date_finish", "trailer_name", "driver_name", "order_key", "booking_id", "date_end_right", 
                "can_edit_chars", "can_select_truck", "can_create_booking", "can_edit_booking"]            
#  COLUMN_KEYS = ["truck_num", "logist_name", "p", "pn", "dr", "d", "r", "pv", "region_release", "city_release", "entry_num", 
#                "contractor_previous_entry", "current_route_entry", "current_status", "current_operation", "logist_comment", 
#                "release_date", "release_time", "coord_last_comment", "next_entry_num", "manager_name", "contractor_next_entry", 
#                "region_loading", "next_route_entry", "loading_date", "loading_time", "tariff", "mileage", "rr_km", "truck_key", "trailer_key", 
#                "driver_key", "date_finish", "trailer_name", "driver_name", "order_key", "booking_id", "date_end_right", 
#                "can_edit_chars", "can_select_truck", "can_create_booking", "can_edit_booking"]            

  before_validation :validate_uniq_truck_id, on: [:create]
  before_update :save_booking_history
  
  belongs_to :booking_type
  belongs_to :user
  belongs_to :contractor
  #has_many :uploading2, class_name: "Uploading2", foreign_key: "truck_id"
  
  validates :booking_type_id, :truck_id, :user_id, :end_date, presence: true

  scope :visible, lambda { where("is_close != ? and status = ?", true, true) }
  scope :active, lambda { visible.where("end_date > ?", Time.now) }

  
  def self.get_mp_data query = nil
    @out = []
    @server = 'sqlsrv01.'
    @i = 0
    begin
      @i +=1
      @ready = Bkg.ready_get_booking?
      @ready = true if @i > 20
      @data = Uploading2.find_by_sql("SELECT a.*, b.id AS booking_id, b.user_id AS booking_user_id, u.id AS user_id, u.last_name AS user_last_name, u.name AS user_name, c.id AS contractor_id, c.short_name,
          rv_t_p.name AS val_p, rv_t_pn.name AS val_pn, rv_t_dr.name AS val_dr, rv_tr_d.name AS val_d, rv_tr_r.name AS val_r,  rv_pv.name AS val_pv 
          FROM #{@server}MonopolySun.dbo.msOrder_OLOP_Get_Table a 
          
          LEFT JOIN #{@server}MonopolySunTemp.dbo.bookings b ON a._TruckKey=b.truck_id
           AND b.is_close != 1 AND b.status=1
          
          LEFT JOIN #{@server}MonopolySunTemp.dbo.users u ON b.user_id=u.id
          
          LEFT JOIN #{@server}MonopolySunTemp.dbo.contractors c ON b.contractor_id=c.id

          LEFT JOIN #{@server}MonopolySunTemp.dbo.resource_characteristic_values rv_t_pn ON a._TruckKey=rv_t_pn.truck_id
           AND rv_t_pn.resource_characteristic_id='0cabe131-ed4c-4aeb-97d3-37b81cf20d15'
           AND getdate() BETWEEN isNULL(rv_t_pn.start_date,'19000101') AND isNULL(rv_t_pn.end_date,'99991231')
           AND rv_t_pn.is_close != 1

          LEFT JOIN #{@server}MonopolySunTemp.dbo.resource_characteristic_values rv_t_p ON a._TruckKey=rv_t_p.truck_id
           AND rv_t_p.resource_characteristic_id='628bd393-f911-4294-8679-2e3fa0ac00ca'
           AND getdate() BETWEEN isNULL(rv_t_p.start_date,'19000101') AND isNULL(rv_t_p.end_date,'99991231')
           AND rv_t_p.is_close != 1

          LEFT JOIN #{@server}MonopolySunTemp.dbo.resource_characteristic_values rv_t_dr ON a._TruckKey=rv_t_dr.truck_id
           AND rv_t_dr.resource_characteristic_id='845c5590-6216-4614-84f5-83aaa47c2f43'
           AND getdate() BETWEEN isNULL(rv_t_dr.start_date,'19000101') AND isNULL(rv_t_dr.end_date,'99991231')
           AND rv_t_dr.is_close != 1

          LEFT JOIN #{@server}MonopolySunTemp.dbo.resource_characteristic_values rv_tr_d ON a._TrailerKey=rv_tr_d.trailer_id
           AND rv_tr_d.resource_characteristic_id='7be1e21f-a84a-4c72-8599-4c457ff889a4'
           AND getdate() BETWEEN isNULL(rv_tr_d.start_date,'19000101') AND isNULL(rv_tr_d.end_date,'99991231')
           AND rv_tr_d.is_close != 1

          LEFT JOIN #{@server}MonopolySunTemp.dbo.resource_characteristic_values rv_tr_r ON a._TrailerKey=rv_tr_r.trailer_id
           AND rv_tr_r.resource_characteristic_id='3e9dfc8e-396f-480c-ab60-74ffad6b6b70'
           AND getdate() BETWEEN isNULL(rv_tr_r.start_date,'19000101') AND isNULL(rv_tr_r.end_date,'99991231')
           AND rv_tr_r.is_close != 1

          LEFT JOIN #{@server}MonopolySunTemp.dbo.resource_characteristic_values rv_pv ON a._DriverKey=rv_pv.driver_id
           AND rv_pv.resource_characteristic_id='a1651a5f-a017-4978-8a01-7db06cc36ab2'
           AND getdate() BETWEEN isNULL(rv_pv.start_date,'19000101') AND isNULL(rv_pv.end_date,'99991231')
           AND rv_pv.is_close != 1
           
          #{query unless query.blank?}
 
          ORDER BY a.region_release, a.city_release, a.truck_num, a.FieldForOrder")  if @ready
    end while !@ready
    @data
  end
  

  
  # ===========================================================================================
  
  def self.get_booking(user, filter = nil, b_menu = nil)
    
    b_menu_query = 
      case b_menu
      when 1 #Транспорт для планирования 
        "(a.part2 = 1) " 
      when 2 #Открытые заявки
        "(a.part1 = 1) " 
      when 3 #Результат планирования 
        "(a.part3 = 1) "
      else
        nil
      end
      
    
     
    @can_edit_chars =  user.admin || user.roles.pluck(:name).include?('Редактор характеристик для обратных загрузок')
    @user_can_select_truck =  user.admin || user.roles.pluck(:name).include?('Бронирование и назначение транспорта')
    @user_can_create_booking =  @user_can_select_truck
    @user_can_edit_booking =  @user_can_select_truck
    
    unless filter.blank?
      @upl_col_names = Uploading2.column_names
      query = ""
      @ind = 0
      filter.each do |f|
        ind = COLUMN_NAMES.index(f[0])
        attr_name = COLUMN_KEYS[ind]
        if @upl_col_names.include?(attr_name) && f[1] != 'Пусто' && f[1] != 'Не пусто'
          case f[2] 
            when 'ИЛИ'
            query += " #{'OR' unless query.blank?} a.#{attr_name} = '#{f[1]}' "
            when 'И'
            query += " #{'AND' unless query.blank?} a.#{attr_name} = '#{f[1]}' "
            when 'И НЕ'
            query += " #{'AND' unless query.blank?} a.#{attr_name} != '#{f[1]}' "
          end
          @ind += 1
        end unless attr_name == 'current_status'
      end
      unless filter.size == @ind
        query = nil 
      else
        query = '(' + query + ')'
      end  
    end
#  @times = []
#  @t = Time.now  

    if !query.blank? and !b_menu_query.blank?
      query = b_menu_query + 'AND ' + query
    elsif !b_menu_query.blank? and query.blank?
    #else
      query = b_menu_query
    end
    
    query = 'WHERE ' + query unless query.blank? 
    
    @data = Booking.get_mp_data query

   
    @data.each do |item|   
#      time = item.current_status.strip != 'Выгружен' && item.entry_num.strip != 'Недоступность' && item._DateFinish - item.utc_region_release.to_i.hours - 3.hours < Time.now ? Time.now + item.utc_region_release.to_i.hours : item._DateFinish unless item._DateFinish.blank?
#      status = 'Готов' if (item.current_status.strip == 'Выгружен') or 
#                          (!item.entry_num.blank? && 
#                            item.entry_num.strip == 'Недоступность' && 
#                            ((item._DateFinish - item.utc_region_release.to_i.hours - 3.hours) < Time.now ) )
      booking = Booking_data.new
        booking.truck_num =                  item.truck_num
        booking.logist_name =                item.logist_name 
        booking.p =  item.val_p ? (item.val_p == '1' ? 'Да' : 'Нет') : nil
        booking.pn = item.val_pn
        booking.dr = item.val_dr ? (item.val_dr == '1' ? 'Да' : 'Нет') : nil
        booking.d =  item.val_d ? (item.val_d == '1' ? 'Да' : 'Нет') : nil
        booking.r =  item.val_r ? (item.val_r == '1' ? 'Да' : 'Нет') : nil
        booking.pv = item.val_pv ? (item.val_pv == '1' ? 'Да' : 'Нет') : nil
        booking.region_release =             item.region_release 
        booking.city_release =               item.city_release 
        booking.entry_num =                  item.entry_num 
        booking.contractor_previous_entry =  item.contractor_previous_entry
        booking.current_route_entry =        item.current_route_entry
#        booking.current_status =             item.current_status = status || item.current_status
        booking.current_status =             item.current_status = item.current_status
        booking.current_operation =          item.current_operation
        booking.logist_comment =             item.logist_comment
        booking.release_date =               item._DateFinish.strftime("%d.%m.%y") unless item._DateFinish.blank?
        booking.release_time =               "#{item._DateFinish.strftime("%k:%M")} (МСК#{Booking.sign_number item.utc_region_release })" unless item._DateFinish.blank?
#        booking.release_date =               time.strftime("%d.%m.%y") unless time.blank?
#        booking.release_time =               "#{time.strftime("%k:%M")} (МСК#{Booking.sign_number item.utc_region_release })" unless time.blank?
        booking.coord_last_comment =         item.coord_last_comment
        booking.next_entry_num =             item.next_entry_num
        booking.manager_name =               (item.manager_name.blank? and item.user_id) ? "#{item.user_last_name} #{item.user_name}" : "#{item.manager_name}"
        booking.contractor_next_entry =      (item.contractor_next_entry.blank? && item.contractor_id) ? item.short_name : item.contractor_next_entry 
        booking.region_loading =             item.region_loading 
        booking.next_route_entry =           item.next_route_entry
        booking.loading_date =               item._DateLoading.strftime("%d.%m.%y") unless item._DateLoading.blank?
        booking.loading_time =               "#{item._DateLoading.strftime("%k:%M")} (МСК#{Booking.sign_number item.utc_region_loading })" unless item._DateLoading.blank?
        booking.tariff =                     item.tariff
        booking.mileage =                    item._mileage
        booking.rr_km =                      item._rr_km
        booking.truck_key =                  item._TruckKey 
        booking.trailer_key =                item._TrailerKey
        booking.driver_key =                 item._DriverKey
        booking.date_finish =                item._DateFinish
        booking.trailer_name =               item._TrailerName
        booking.driver_name =                item._DriverName
        booking.order_key =                  item._OrderKey
        booking.booking_id =                 item.id
        booking.date_end_right =             item._DateEndRight
        booking.can_edit_chars =             @can_edit_chars
        booking.can_select_truck =           @user_can_select_truck && item.icon_t
        booking.can_create_booking =         @user_can_create_booking && item.icon_b
        booking.can_empty_mileage  =         item.icon_p
        booking.can_edit_booking =           @user_can_edit_booking && item.booking_id && (item.booking_user_id == user.id) 
        booking.expensive =                  item.expensive
        booking.color_release_date =         item.color_info
        booking.part =                       "#{'Открытые заявки' if item.part1}:#{'Транспорт для планирования' if item.part2}:#{'Результат планирования' if item.part3}"
        booking.citykey_release =            item.citykey_release
          
        @out << booking
    end if @data
    @out

#  @times << (Time.now - @t)
#  "Time: #{@times.sum.to_f / @times.size}, size: #{@data.size}"
   #[@out.try(:size), @ready, (Time.now - @t)]
  end
  
  
  
  # ===========================================================================================
  
  def self.get_column_values(col_name) 
      
    @data = Booking.get_mp_data
    
    @data.each do |item|   
      time = item.current_status.strip != 'Выгружен' && item.entry_num.strip != 'Недоступность' && item._DateFinish - item.utc_region_release.to_i.hours < Time.now ? Time.now + item.utc_region_release.to_i.hours : item._DateFinish unless item._DateFinish.blank?
      status = 'Готов' if (item.current_status.strip == 'Выгружен') or 
                          (item.entry_num == 'Недоступность' && (item._DateFinish - item.utc_region_release.to_i.hours < Time.now ) )

      booking = Booking_data.new
        booking.truck_num =                  item.truck_num 
        booking.logist_name =                item.logist_name 
        booking.p =  item.val_p ? (item.val_p == '1' ? 'Да' : 'Нет') : nil
        booking.pn = item.val_pn
        booking.dr = item.val_dr ? (item.val_dr == '1' ? 'Да' : 'Нет') : nil
        booking.d =  item.val_d ? (item.val_d == '1' ? 'Да' : 'Нет') : nil
        booking.r =  item.val_r ? (item.val_r == '1' ? 'Да' : 'Нет') : nil
        booking.pv = item.val_pv ? (item.val_pv == '1' ? 'Да' : 'Нет') : nil
        booking.region_release =             item.region_release 
        booking.city_release =               item.city_release 
        booking.entry_num =                  item.entry_num 
        booking.contractor_previous_entry =  item.contractor_previous_entry
        booking.current_route_entry =        item.current_route_entry
        booking.current_status =             item.current_status = status || item.current_status
        booking.current_operation =          item.current_operation
        booking.logist_comment =             item.logist_comment
        booking.release_date =               time.strftime("%d.%m.%y") unless time.blank?
        booking.release_time =               "#{time.strftime("%k:%M")} (МСК#{Booking.sign_number item.utc_region_release })" unless time.blank?        
        booking.coord_last_comment =         item.coord_last_comment
        booking.next_entry_num =             item.next_entry_num
        booking.manager_name =               (item.manager_name.blank? and item.user_id) ? "#{item.user_last_name} #{item.user_name}" : "#{item.manager_name}" 
        booking.contractor_next_entry =      (item.contractor_next_entry.blank? && item.contractor_id) ? item.short_name : item.contractor_next_entry 
        booking.region_loading =             item.region_loading         
        booking.next_route_entry =           item.next_route_entry
        booking.loading_date =               item._DateLoading.strftime("%d.%m.%y") unless item._DateLoading.blank?
        booking.loading_time =               "#{item._DateLoading.strftime("%k:%M")} (МСК#{Booking.sign_number item.utc_region_loading })" unless item._DateLoading.blank?
        booking.tariff =                     item.tariff
            
        @out << booking
    end if @data

    ind = COLUMN_NAMES.index(col_name)
    attr_name =  COLUMN_KEYS[ind]

    @current_column_values =  @out.uniq! { |o| o.send(attr_name)}.map { |u| u.send(attr_name).to_s.strip.delete("'") unless u.send(attr_name).to_s.blank? }.compact.sort
    @current_column_values.sort! { |a, b| Booking.is_date(a) <=> Booking.is_date(b) } if attr_name == 'release_date' || attr_name == 'loading_date'
    @current_column_values << 'Не пусто' unless @current_column_values.blank?
    @current_column_values << 'Пусто'
    
    @current_column_values
  end
  
  # ===========================================================================================
  
  def self.push_booking
    connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLOP_Get_Fill] @FromSite=1")
  end
    
  def self.set_empty_mileage(order_key, truck_key, user_id, dest_from, dest_to, time_from, time_to, contractor, loading_date)
    url = "http://esbsrv01.monopoly.su/ESB/ST/Write/SetOrder.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{ 'OrderKey' : '#{order_key}', 'TruckKey' : '#{truck_key}' , 'UserId' : '#{user_id}', 'DestinationFromKey' : '#{dest_from}', 'DestinationToKey' : '#{dest_to}', 'MomentFrom' : '#{time_from}', 'MomentTo' : '#{time_to}', 'CustomerKey': '#{contractor}', 'LoadingDate' : '#{loading_date}' }" 
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  def self.get_city_locations(city_key)
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetDestinationByCityKey.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'CityKey':'#{city_key}'}"
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  def self.get_regions# (city_release)
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetCitiesWithRegion.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    #request.body =  "{'CityName' : '#{city_release}'}"
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  
  def self.get_trucks
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetTrucks.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  def self.get_trucks_test
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetTrucks.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    response = http.request(request)
  
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  def self.set_truck(order, truck, user)
    url = "http://esbsrv01.monopoly.su/ESB/ST/Write/SetTrucks.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'OrderKey' : '#{order}', 'TruckKey' : '#{truck}' , 'UserId' : '#{user}'}"
    response = http.request(request)
  
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end


  def self.tst_get_booking
    connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLOP_Get]").to_a
  end

  def self.sign_number num
    num >= 0 ? "+#{num}" : num.to_s if num
  end
  
  
  def self.find_booking(truck_id)
    unless truck_id.blank?
      booking = Booking.visible.where(truck_id: truck_id).limit(1).select(:contractor_id, :user_id)
      booking.first.blank? ? nil : booking.first
    end  
  end
  
  
  
  def self.is_date(str)
    begin
      DateTime.strptime(str, '%d.%m.%y').to_time.to_i.to_s
    rescue # ArgumentError
      str.to_s
    end
  end
  
  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end

  class Bkg < Monosql
    def self.ready_get_booking?
      connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLOP_CheckReady]").to_a.first['IsOk']
    end
  end
    

  
private
  
  
  def save_booking_history
    @old_booking = Booking.find(self.id)
    BookingHistory.create(
      booking_id: self.id, 
      status: self.status, 
      status_start_date: @old_booking.start_date, 
      status_end_date: @old_booking.end_date, 
      status_user_id: @old_booking.status_user_id)
  end
  
  def validate_uniq_truck_id
    if Booking.visible.where(truck_id: self.truck_id).any?
      errors.add(:booking, "Бронь создана другим пользователем")
      false
    else
      true
    end  
  end
  

  
  
end
