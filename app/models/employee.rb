class Employee < ActiveRecord::Base
  #  attr_accessible :employment_date, :end_date, :id, :leaving_date, :person_id, :start_date, :staff_unit_id, :is_close

  belongs_to :person
  belongs_to :staff_unit
  has_many :unv_employees
  has_many :truck_drivers
  has_many :process_action_accesses
  has_many :task_action_accesses
  has_many :contractor_responsibles

  #audited

  #before_validation :ensure_uuid #, :delete_empty_parameters

  validates :employment_date, :person_id, :staff_unit_id,  presence: true

  scope :visible, lambda { where("employees.is_close != ?", true) }
  scope :sailer, lambda { visible.where("employees.staff_unit_id = ?", "05c34cd6-51c5-4423-9837-e01a01226996") }
  scope :closed_employees, lambda { where("employees.is_close = ?", true) }
  scope :driver_expeditors, lambda { visible.joins(:staff_unit, :person).where("staff_units.position_id = ?", '052f55d0-6277-47af-80c9-efdfbc75b1cc').where("people.id_zup IS NOT NULL or people.id_zup_logistik IS NOT NULL or people.id_zup_mtk IS NOT NULL").order("people.last_name") }
  #scope :truck_drivers, lambda { unscoped.map { |e| e if e.truck_drivers.any? }.compact.uniq.sort_by(&:person_info) }
  scope :used_drivers, lambda { driver_expeditors.joins(:truck_drivers).where("truck_drivers.end_date > ? OR (truck_drivers.start_date IS NOT NULL AND truck_drivers.end_date IS NULL)", Time.now).includes(truck_drivers: :transport) }
  scope :free_drivers, lambda { driver_expeditors - used_drivers }
  #scope :free_drivers, lambda { driver_expeditors.includes(:truck_drivers).where("truck_drivers.employee_id IS NULL OR truck_drivers.end_date > ? OR (truck_drivers.employee_id IS NOT NULL AND truck_drivers.end_date IS NULL)", Time.now) }

  def close_employee
    self.update_attribute( :is_close, true )
    self.update_attribute( :leaving_date, Time.now ) unless self.leaving_date
    EmployeeHistory.create({employee_id: self.id,
        employment_date: self.employment_date,
        leaving_date: self.leaving_date,
        start_date: self.start_date,
        end_date: self.end_date,
        contractor_id: self.staff_unit.department.contractor_id,
        contractor_name: self.try("staff_unit.department.contractor.name"),
        department_id: self.staff_unit.department_id,
        department_name: self.staff_unit.department.name,
        position_id: self.staff_unit.position_id,
        position_name: self.staff_unit.position.name,
        person_id: self.person_id,
        staff_unit_id: self.staff_unit_id })
  end


  def unclose
    self.update_attribute( :is_close, false )
  end



  #  def cdp #contractor + department + position
  #    self.staff_unit ?  "Контрагент:#{self.staff_unit.department.contractor.name}. Подразделение: #{self.staff_unit.department.name}. Должность: #{self.staff_unit.position.name}." : false
  #  end

  def persons
    self.map { |p| p.person }
  end


  def person_fullname
    person.fullname if person
  end

  def person_info
    "#{person.fullname}, #{person.birthdate}" if person
  end

  def position
    staff_unit.position.name if staff_unit and staff_unit.position
  end

  def person_position
    "#{person_info}, #{position}"
  end
  
  private


  def ensure_uuid
    self.id ||= SecureRandom.uuid
  end

  def delete_empty_parameters
    Employee.accessible_attributes.each do |c|
      self[c.to_sym] = nil if self[c.to_sym].blank? and  c!= 'is_close'
    end
  end


end
