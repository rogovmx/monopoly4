class ContractorAnalysisParameter < ActiveRecord::Base
  
  has_many :contractor_analysis_parameter_changes
  
  validates :contractor_analysis_parameter_id, :parameter_value, :start_date, presence: true
  
  
end
