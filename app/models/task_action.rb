class TaskAction < ActiveRecord::Base

  has_many :task_action_accesses

  scope :visible, lambda { where("[task_actions].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
