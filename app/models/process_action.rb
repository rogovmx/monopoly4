class ProcessAction < ActiveRecord::Base

  has_many :process_action_accesses

  scope :visible, lambda { where("[process_actions].[is_close] != ?", true) }


  def close
    self.update_attribute( :is_close, true )
  end

  def unclose
    self.update_attribute( :is_close, false )
  end


end
