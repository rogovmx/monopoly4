require 'unv'

class Unavailable < Monosql

  def self.getdata(user_id = nil)
    #connection.select_all "exec [mono-sql].[MonopolySun].[dbo].[ms_Order_OLTO_Get]"
    user_id = "'" + user_id + "'" if user_id
    connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLTO_Get] @idUser = #{user_id ? user_id : 'NULL'}").to_a
  end

  def self.getdata_ol
    connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLTO_Get_OL]").to_a
  end

  def self.get_document1_ol(tr_key)
    tr_key = "'" + tr_key + "'"
    connection.select_all("exec [MonopolySun].[dbo].[ms_Document1_Get] @TransportKey = #{tr_key}").to_a
  end

  def self.get_document2_ol(tr_key)
    tr_key = "'" + tr_key + "'"
    connection.select_all("exec [MonopolySun].[dbo].[ms_Document2_Get] @TransportKey = #{tr_key}").to_a
  end

  def self.get_document3_ol(tr_key)
    tr_key = "'" + tr_key + "'"
    connection.select_all("exec [MonopolySun].[dbo].[ms_Document3_Get] @TransportKey = #{tr_key}").to_a
  end

  def self.repair_request(tr_key, chain=nil)
    tr_key = "'" + tr_key + "'"
    connection.select_all("exec [MonopolySun].[dbo].[ms_TransportRepairRequest_Get] @TransportKey = #{tr_key}, @ForChain = #{chain}").to_a
  end

  def self.relocate_request(tr_key)
    tr_key = "'" + tr_key + "'"
    connection.select_all("exec [MonopolySun].[dbo].[ms_TransportPereezd_Get] @TransportKey = #{tr_key}").to_a
  end

  def self.connection_request(from, to)
    connection.select_all("exec [MonopolySun].[dbo].[ms_CheckConcat_List] @MomentFrom = #{from ? from : 'NULL'}, @MomentTo = #{to ? to : 'NULL'}").to_a
  end

  def self.connections_unv(key)
    key = "'" + key + "'"
    connection.select_all("exec [MonopolySun].[dbo].[ms_Inaccessibility_Get] @OrderKey = #{key}").to_a
  end

  def self.uploading
    connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLOP_Get]").to_a
  end

  def self.uploading2
    @out = []
    @data = connection.select_all("exec [MonopolySun].[dbo].[ms_Order_OLOP_Get]").to_a
    @col_names =  @data.first.try(:keys)
    @data.map!{|u| u.values.map!{|u2| u2.to_s}}
    @data.each do |item|   
      upl = Uploading.new(
      logist_name: item[0],
      truck: item[1],
      city_release: item[2],
      current_client_route: item[3],
      current_status: item[4],
      release_date: item[5],
      release_time: item[6],     
      logist_comment: (!item[7].blank? and item[7].index('#')) ? item[7][0..item[7].index('#')-1].strip.delete("'") : '',
      manager_name: item[8],
      tariff: item[9],
      mileage: item[10],
      rr_km: item[11],
      manager_comment: (!item[12].blank? and item[12].index('#')) ? item[12][0..item[12].index('#')-1].strip.delete("'") : '',
      logist_comment_link: (!item[7].blank? and item[7].index('#')) ? item[7][item[7].index('#')+1..-1].strip.delete("'") : nil,
      manager_comment_link: (!item[12].blank? and item[12].index('#')) ? item[12][item[12].index('#')+1..-1].strip.delete("'") : nil
      )      
      @out << upl
    end
    [@col_names, @out]
  end

  def self.connection_request2(from = 'NULL' , to = 'NULL', user_id = 'NULL')
    user_id = "'" + user_id + "'" if user_id and user_id != 'NULL'
    @data = connection.select_all("exec [MonopolySun].[dbo].[ms_CheckConcat_List] @MomentFrom = #{from ? from : 'NULL'}, @MomentTo = #{to ? to : 'NULL'}, @user_id = #{user_id ? user_id : 'NULL'}").to_a
    @out = []
    @col_names =  @data.first.try(:keys)
    @data.map!{|u| u.values.map!{|u2| u2.to_s}}
    @data.each do |item|   
      upl = Connection.new
        upl.order_from_key = item[0]
        upl.order_to_key =  item[1]
        upl.truck_num =  item[2]
        upl.logist =  item[3]
        upl.previous_entry =  item[4]
        upl.previous_entry_city =  item[5]
        upl.previous_entry_unloading_time =  item[6]
        upl.previous_entry_actual_time =  item[7]
        upl.unv =  (!item[8].blank? and item[8].index('#')) ? item[8][0..item[8].index('#')-1].strip.delete("'") : ''
        upl.conn_time =  item[9]
        upl.next_entry =  item[10]
        upl.next_entry_city =  item[11]
        upl.manager =  item[12]
        upl.contractor = item[13]
        upl.s_time_1op_next_entry =  item[14]
        upl.next_entry_mileage =  item[15]
        upl.is_limit_exc =  item[16]
        upl.next_entry_created_at =  item[17]
        upl.unv_link =  (!item[8].blank? and item[8].index('#')) ? item[8][item[8].index('#')+1..-1].strip.delete("'") : nil 
      
      @out << upl
    end
    [@col_names, @out]
  end
  
  
  #
  #  def self.getdata
  #    self.connection.execute("[mono-sql].[MonopolySun].[dbo].[ms_Order_OLTO_Get]")
  #  end


end


