#$ ->
#  $(":checkbox").checkboxpicker()
#  return


notice = (note) ->
  $.jGrowl note if !!note
  return
window.notice = notice


nestsort = ->
  $ ->
    $("#sortable").nestedSortable
      listType: "ul"
      items: "li"
      placeholder: "highlight"
      forcePlaceholderSize: true
      handle: "div"
      helper: "clone"
      opacity: .6
      revert: 250
      tabSize: 25
      tolerance: "pointer"
      toleranceElement: "> div"

    $("#sortable").disableSelection() # make links not clickable
    $("#serialize").click ->
      csrf()
      c = set: JSON.stringify($("#sortable").nestedSortable("toHierarchy",
        startDepthCount: 0
      ))
      $.post "functionalgroups/savesort", c, $.jGrowl("Ok")        
      false
    return
  return
window.nestsort = nestsort  
  
csrf = ->
  $(document).ajaxSend (e, xhr, options) ->
    token = $("meta[name='csrf-token']").attr("content")
    xhr.setRequestHeader "X-CSRF-Token", token
    return
  return
window.csrf = csrf    
  
$(document).ready ->
  $("title").append $("h2:first").text()
  return

$(document).ready ->
  $("#popup").draggable handle: "#popup_head"
  $("#filter_and_search").draggable()
  return

$(document).ready ->
  $('.select_date').datetimepicker format:'d.m.Y'
  return

$(document).ready ->
  $("#popup_close").click ->
    $("#popup_content").html ""
    $("#popup").hide()
    return

  return

$(document).ready ->
  $(".input_phone").mask "9(999)999-99-99",
    placeholder: "#"

  $("#snils").mask "999-999-999 99"
  $("#mobile").mask "7(999) 999 9999"
  $(".mobile").mask "7(999) 999 9999"
  return


# +++++++++++++++++++++++++++++++++ DataTable
$(document).ready ->
  $("#truck_responsibles").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[ 20, 50, 100, -1], [ 20, 50, 100, "Все"]]
  return
  
$(document).ready ->
  $("#contractor_responsibles").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[ 20, 50, 100, -1], [ 20, 50, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [3, 4]]
  return
  
$(document).ready ->
  $("#transportmodel1cs").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[ 30, 60, 100, -1], [ 30, 60, 100, "Все"]]
  return
  
$(document).ready ->
  $("#contacts").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[ 30, 60, 100, -1], [ 30, 60, 100, "Все"]]
  return
  
$(document).ready ->
  $("#users").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[ 30, 60, 100, -1], [ 30, 60, 100, "Все"]]
  return

$(document).ready ->
  $("#columns").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [6, 7]]

  return

$(document).ready ->
  $("#contract_kind_roles").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#contract_kinds").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4]]

  return

$(document).ready ->
  $("#contract_relationships").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4]]

  return

$(document).ready ->
  $("#contract_subkinds").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#contract_types").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#contractor_representatives").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6, 7]]

  return

$(document).ready ->
  $("#contractors").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [10, 11, 12]]

  return

$(document).ready ->
  $("#contractortypes").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3, 4]]

  return

$(document).ready ->
  $("#contracts").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6, 7]]

  return

$(document).ready ->
  $("#couplers").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5, 6]]

  return


#$(document).ready(function() {
#  $('#departments').dataTable({
#    "order": [[0, "asc"]],
#    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "Все"]],
#    "aoColumnDefs": [{"bSortable": false, "aTargets": [5, 6, 7]}]
#  });
#});
$(document).ready ->
  $("#documentkinds").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5, 6, 7]]

  return

$(document).ready ->
  $("#documents").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6, 7, 8]]

  return

$(document).ready ->
  $("#documenttypes").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4, 5]]

  return

$(document).ready ->
  $("#duties").dataTable
    order: []
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]

  return

$(document).ready ->
  $("#employee_histories").dataTable
    order: []
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [10, 11, 12]]

  return

$(document).ready ->
  $("#employees").dataTable
    order: []
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6, 7]]

  return

$(document).ready ->
  $("#enginemodels").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#enginetypes").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#objecttypeproperties").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [6, 7, 8]]

  return

$(document).ready ->
  $("#objecttypepropertycolumns").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5, 6]]

  return

$(document).ready ->
  $("#objecttypes").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5, 6]]

  return

$(document).ready ->
  $("#people").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]

  return

$(document).ready ->
  $("#phones").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#phoneuses").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6, 7]]

  return

$(document).ready ->
  $("#positions").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#resourse_types").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#roles").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#staff_units").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [6, 7, 8]]

  return

$(document).ready ->
  $("#systems").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6]]

  return

$(document).ready ->
  $("#tables").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6]]

  return

$(document).ready ->
  $("#transportbrands").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#transportcategories").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4]]

  return

$(document).ready ->
  $("#transportcolors").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [1, 2]]

  return

$(document).ready ->
  $("#transportecologicals").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [1, 2]]

  return

$(document).ready ->
  $("#transportexploitations").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#transportkinds").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [5, 6]]

  return

$(document).ready ->
  $("#transportloadings").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3]]

  return

$(document).ready ->
  $("#transportmodels").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]

  return

$(document).ready ->
  $("#transports").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[30, 50, 100, -1], [30, 50, 100, "Все"]]

  return

$(document).ready ->
  $("#transporttanks").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]

  return

$(document).ready ->
  $("#transporttypes").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#truck_drivers").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5]]

  return

$(document).ready ->
  $("#unv_employees").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [6, 7, 8]]

  return

$(document).ready ->
  $("#unv_types").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4]]

  return

$(document).ready ->
  $("#task_types").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [2, 3, 4]]

  return

$(document).ready ->
  $("#task_templates").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [6, 7, 8]]

  return

$(document).ready ->
  $("#process_types").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4, 5]]

  return

$(document).ready ->
  $("#process_templates").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [4, 5, 6]]

  return

$(document).ready ->
  $("#task_actions").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [bSortable: false, aTargets: [3, 4, 5]]

  return

$(document).ready ->
  $("#agreements").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
  return

$(document).ready ->
  $("#agreement_accesses").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
  return

$(document).ready ->
  $("#agreement_requests").dataTable
    order: [[3, "desc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]
    aoColumnDefs: [[bSortable: false, aTargets: [2]], [bSearchable: false, aTargets: [2]]]
 
  return

$(document).ready ->
  $("#report_table").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]

  return

$(document).ready ->
  $("#fillup").dataTable
    order: [[1, "asc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]

  return

$(document).ready ->
  $("#unavailable_resources").dataTable
    bFilter: false
    order: [[0, "asc"]]
    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "Все"]]

  return


$(document).ready ->
  $("#vacations").dataTable
    order: [[3, "desc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [3, 4]]
 
  return

$(document).ready ->
  $("#vacations_leave").dataTable
    order: [[4, "desc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [3, 4]]
 
  return

$(document).ready ->
  $("#unv_employees_not_return").dataTable
    order: [[3, "desc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [3, 4]]
 
  return

$(document).ready ->
  $("#vacations_return").dataTable
    order: [[4, "desc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [4, 5]]
 
  return

$(document).ready ->
  $("#gps_nav_transports").dataTable
    order: [[0, "desc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [2, 3]]
 
  return

$(document).ready ->
  $("#coordinators").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
 
  return

$(document).ready ->
  $("#trucks_by_azs_litres").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
 
  return

$(document).ready ->
  $("#azs_litres").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
 
  return

$(document).ready ->
  $("#azs").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
 
  return

$(document).ready ->
  $("#coordinators_by_azs_litres").dataTable
    order: [[0, "asc"]]
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
 
  return

$(document).ready ->
  $("#processes_group_by_truck_and_inaccess").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    order: [[0, "asc"]]
    columnDefs: [targets: [0], orderData: [0, 1]]
    
  return

$(document).ready ->
  $("#processes_group_by_inaccess").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    order: [[0, "asc"]]
    
  return

$(document).ready ->
  $("#processes_group_by_truck").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    order: [[0, "asc"]]
    
  return

$(document).ready ->
  $("#processes_with_waiting").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [{type: 'date-euro', targets: [1, 2]}, {targets: [ 0 ], orderData: [ 0, 1 ]}]
    
  return

$(document).ready ->
  $("#processes_without_overlap").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [{type: 'date-euro', targets: [1, 2]}, {targets: [0], orderData: [0, 1]}]
    
  return

$(document).ready ->
  $("#processes_without_nesting").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [{type: 'date-euro', targets: [1, 2]}, {targets: [ 0 ], orderData: [ 0, 1 ]}]
    
  return

$(document).ready ->
  $("#all_processes").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [{type: 'date-euro', targets: [1, 2]}, {targets: [ 0 ], orderData: [ 0, 1 ]}]
    
  return

$(document).ready ->
  $("#time_zones").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [2, 3]]
    
  return

$(document).ready ->
  $("#truks").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [1, 2, 5, 6]]
    
  return

$(document).ready ->
  $("#modulators").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    
  return

$(document).ready ->
  $("#extra_spendings").dataTable
    lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "Все"]]
    columnDefs: [type: 'date-euro', targets: [3, 9]]
  return


