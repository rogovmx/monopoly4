module ApplicationHelper
  
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  # вывод марки, модели, vin и рег. номера ТС
  def transport(transport)
    if transport
      @transport = transport
      if transport.transportmodel
        @transportmodel = transport.transportmodel.name
        @transportbrand = transport.transportmodel.transportbrand.name
      end
      render "shared/transport"
    end
  end

  # вывод физ. лица
  def person(person)
    if person
      @person = person
      render "shared/person"
    end
  end

  def registration_coupon?(document)
    return (document.documentkind and document.documentkind.id.to_s == '3be7c8f6-fae8-4090-b67a-697d000c930b'.upcase) ? true : false

  end

  def person_phones(person)
    @person = person
    @phones = @person.phones
    render "shared/person_phones"
  end



  def document_attr(documentkind, attr)
    return {value: 1, checked: "checked",} if documentkind.documentattr and documentkind.documentattr.send(attr)
    0
  end

  def document_attr?(document, attr)
    return true if document.documentkind and document.documentkind.documentattr and document.documentkind.documentattr.send(attr)
    false
  end


  def letter_pagination letters
    render partial: "shared/letter_pagination" , locals: {letters: letters, current_letter: params[:letter] || letters[0] }
  end

  
  def select_date_from_to(url = 'index', from = 3.days.ago, to = 3.days.since)
    @default_from = from
    @default_to = to
    @url = url
    render 'shared/select_date_from_to'
  end
  
  def save_reports_xls(title, data)
    @title = title
    @data = data
    render 'shared/save_reports_xls'
  end
  
#  def pagination_links(collection, options = {})
#   options[:renderer] ||= BootstrapPaginationHelper::LinkRenderer
#   options[:class] ||= 'pagination pagination-centered'
#   options[:inner_window] ||= 2
#   options[:outer_window] ||= 1
#   will_paginate(collection, options)
# end

  def show_table(table_data)
   
     if table_data['Ok']
      table_title = table_data['Data']['Head']
      table_body_all_data = table_data['Data']['Body']
      
      @invisible_columns = table_title.map.with_index { |t, ind| ind unless t['Visible'] }.compact
      @date_columns = table_title.map.with_index { |t, ind| ind if t['Visible'] and t['DataType'] == 'datetime' }.compact
      
      
      @table_column_names = table_title.map { |t| t['Title'] if t['Visible'] }.compact
      @table_body = table_body_all_data.each { |row| row.delete_if.with_index { |col, ind| @invisible_columns.include?(ind) }  }
    else
      @error = table_data['ErrorMessage'] 
    end
    render 'shared/show_table'
  end

  def show_table2(table_data)

      table_title = table_data['Head']
      table_body_all_data = table_data['Body']
      
      @invisible_columns = table_title.map.with_index { |t, ind| ind unless t['Visible'] }.compact
      @date_columns = table_title.map.with_index { |t, ind| ind if t['Visible'] and t['DataType'] == 'datetime' }.compact
      
      
      @table_column_names = table_title.map { |t| t['Title'] if t['Visible'] }.compact
      @table_body = table_body_all_data.each { |row| row.delete_if.with_index { |col, ind| @invisible_columns.include?(ind) }  }

    render 'shared/show_table'
  end
  

end
