module WillPaginateHelper
  class WillPaginateAjaxLinkRenderer < WillPaginate::ActionView::LinkRenderer
    def prepare(collection, options, template)
      options[:params] ||= {}
      options[:params]["_"] = nil
      super(collection, options, template)
    end

    protected
    def link(text, target, attributes = {})
      if target.is_a? Fixnum
        attributes[:rel] = rel_value(target)
        target = url(target)
        target = target.gsub("index", "save_param") if target.index('uploadings') or target.index('monitoring_connections') or target.index('bookings')
        target = target.gsub("get_bookings", "save_param") if target.index('bookings')
        target = target.gsub("transport_planning", "save_param") if target.index('transport_planning')
        target = target.gsub("open_orders", "save_param") if target.index('open_orders')
        target = target.gsub("planned", "save_param") if target.index('planned')
      end
      ajax_call = "$.ajax({url: '#{target}', dataType: 'script'});"
      @template.link_to_function(text.to_s.html_safe, ajax_call, attributes)
    end
  end

  def ajax_will_paginate(collection, options = {})
    will_paginate(collection, options.merge(:renderer => WillPaginateHelper::WillPaginateAjaxLinkRenderer))
  end
  
  def link_to_function(name, *args, &block)
     html_options = args.extract_options!.symbolize_keys

     function = block_given? ? update_page(&block) : args[0] || ''
     onclick = "#{"#{html_options[:onclick]}; " if html_options[:onclick]}#{function}; return false;"
     href = html_options[:href] || '#'

     content_tag(:a, name, html_options.merge(:href => href, :onclick => onclick))
  end
  
end