module TransportsHelper
  
  def can_edit_locked_coupler?(coupler)
    !coupler.locked_to_date? || (coupler.locked_to_date? && user_can_edit_locked_coupler?(coupler))
  end
  
  def user_can_edit_locked_coupler?(coupler)
    current_user.admin || current_user.roles.pluck(:name).include?('Блокировка сцепок')
  end
  
  
end
