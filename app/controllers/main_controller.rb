class MainController < ApplicationController
  skip_before_action :user_permitted_functionality
  
  def index
    @users = User.all if user_signed_in? and current_user.admin
  end

  def change_user
    sign_in :user, User.find(params[:id])
    #SendMail.msg("Сообщение").deliver
    redirect_to :back
  end

end
