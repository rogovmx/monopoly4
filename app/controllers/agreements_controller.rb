class AgreementsController < ApplicationController
  before_action :set_agreement, only: [:edit, :update, :destroy]


  def index
    @agreements = Agreement.all
  end


  def new
    @agreement = Agreement.new
  end

  def edit
  end

  def create
    @agreement = Agreement.new(agreement_params)

    if @agreement.save
      redirect_to agreements_url, notice: 'Вариант согласования добавлен'
    else
      render :new
    end
  end

  def update
    if @agreement.update(agreement_params)
      redirect_to agreements_url, notice: 'Вариант согласования сохранен'
    else
      render :edit
    end
  end

  def destroy
    @agreement.destroy
    redirect_to agreements_url, notice: 'Вариант согласования удален'
  end

  private
  def set_agreement
    @agreement = Agreement.find(params[:id])
  end

  def agreement_params
    params.require(:agreement).permit(:name, :descr, :is_close, :lifetime, :uses_number, :time_use)
  end
end
