class Transportmodel1csController < ApplicationController
  before_action :set_transportmodel1c, only: [:edit, :update, :destroy]


  def index
    @transportmodel1cs = Transportmodel1c.all
  end

  def edit
  end


  def update
    if @transportmodel1c.update(transportmodel1c_params)
      redirect_to transportmodel1cs_url, notice: 'Модель ТС 1С сохранена'
    else
      render :edit
    end
  end

  def destroy
    @transportmodel1c.close
    redirect_to transportmodel1cs_url, notice: 'Модель ТС 1С удалена'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transportmodel1c
      @transportmodel1c = Transportmodel1c.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def transportmodel1c_params
      params.require(:transportmodel1c).permit(:transportmodel_id, :is_close)
    end
end
