
class TempsController < ApplicationController
  #include ActionController::Live
  
  skip_before_action :authenticate_user!, :only => [:index]

  def strm
    respond_to :html
  end
  
  def strm2
    response.headers['Content-Type'] = 'text/event-stream'
    10.times {
      data = {time: Time.now.to_s}.to_json
      response.stream.write "event: update\n"
      response.stream.write "data: #{data}\n\n"
      sleep 3
    }
  ensure
    response.stream.close
  end
  
  
  
  
  
  
  def index
    #@table_data = get_table_data('http://Esbsrv01/ESB/Reports/TrucksAtBase.ashx')
  
  end





  def show
   # neworder = JSON.parse(params[:params])
    render text: request.env["REMOTE_USER"]
  end

  def new
    require 'net/http'
    require 'uri'
    require 'json'


  #url = 'https://hst-api.wialon.com/wialon/ajax.html?svc=core/login&sid='
  url = 'https://hst-api.wialon.com/wialon/ajax.html?svc=core/login'
  params = {params:{user:"monopoly",password:"123456"}}.to_json
  
    uri = URI.parse(url)
    headers={'content-type'=>'application/x-www-form-urlencoded'}
    request = Net::HTTP::Post.new(uri.request_uri, headers)
    request.body = params
  @resp, @body =   Net::HTTP.new(uri.host,uri.port).start{|http| http.request(request)}

 render text: "/#{@resp.to_s} / #{@body}/"


  end

  # GET /temps/1/edit
  def edit

#  if ldap.bind
#    @a = ldap.search(
#      base:         "DC=monopoly",
#      #filter:       Net::LDAP::Filter.eq( "mail", 'mihail.rogov@monopoly.su' ),
#      filter:      'mail=mihail.rogov@monopoly.su',
#      return_result: true
#    ).first
#  end
  
    render text: ldap_search.attribute_names
  #render text: ldap_search.dn
  #render text: "#{ldap_init.bind}"
 
  end
  
  def ldap_init
        @host = 'dc01.monopoly.su'
        @port = 389
        @email = 'mihail.rogov@monopoly.su'
        @password = 'OyLU83eTnhV'

        ldap = Net::LDAP.new
          ldap.host = @host
          ldap.port = @port
          ldap.auth @email, @password
        ldap
  end



     def ldap_search
        search = ldap_init.search(
        base:         "DC=monopoly,DC=su",
        #filter:       Net::LDAP::Filter.eq( "mail", 'mihail.rogov@monopoly.su' ),
        #filter:       'mail=mihail.rogov@monopoly.su',
        filter:      'samaccountname=mihail.rogov',
        return_result: true
        )
        search.first
      end



  # POST /temps
  # POST /temps.json
  def create
    @temp = Temp.new(params[:temp])

    respond_to do |format|
      if @temp.save
        format.html { redirect_to @temp, notice: 'Temp was successfully created.' }
        format.json { render json: @temp, status: :created, location: @temp }
      else
        format.html { render action: "new" }
        format.json { render json: @temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /temps/1
  # PUT /temps/1.json
  def update
    @temp = Temp.find(params[:id])

    respond_to do |format|
      if @temp.update_attributes(params[:temp])
        format.html { redirect_to @temp, notice: 'Temp was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @temp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /temps/1
  # DELETE /temps/1.json
  def destroy
    @temp = Temp.find(params[:id])
    @temp.destroy

    respond_to do |format|
      format.html { redirect_to temps_url }
      format.json { head :no_content }
    end
  end


 private

 def temp_params
   params.require(:temp).permit(:num1, :str1)
 end
 

end
