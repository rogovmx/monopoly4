class TemplateStepsController < ApplicationController
  before_action :set_template_step, only: [:show, :edit, :update, :destroy]


  def index
    @template_steps = TemplateStep.all
  end

  def show
  end

  def new
    @template_step = TemplateStep.new
  end

  def edit
  end

  def create
    @template_step = TemplateStep.new(template_step_params)

    if @template_step.save
      redirect_to @template_step, notice: 'Template step добавлен'
    else
      render :new
    end
  end

  def update
    if @template_step.update(template_step_params)
      redirect_to @template_step, notice: 'Template step сохранен'
    else
      render :edit
    end
  end

  def destroy
    @template_step.destroy
    redirect_to template_steps_url, notice: 'Template step удален'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template_step
      @template_step = TemplateStep.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def template_step_params
      params.require(:template_step).permit(:name, :process_template_id, :task_template_id, :parent_id, :parent2_id, :rgt, :lft, :is_close)
    end
end
