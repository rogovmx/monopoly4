class TaskStatusesController < ApplicationController
  before_action :set_task_status, only: [:show, :edit, :update, :destroy]


  def index
    @task_statuses = TaskStatus.all
  end

  def show
  end

  def new
    @task_status = TaskStatus.new
  end

  def edit
  end

  def create
    @task_status = TaskStatus.new(task_status_params)

    if @task_status.save
      redirect_to @task_status, notice: 'Состояние задачи добавлено'
    else
      render :new
    end
  end

  def update
    if @task_status.update(task_status_params)
      redirect_to @task_status, notice: 'Состояние задачи сохранено'
    else
      render :edit
    end
  end

  def destroy
    @task_status.destroy
    redirect_to task_statuses_url, notice: 'Состояние задачи удалено'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task_status
    @task_status = TaskStatus.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def task_status_params
    params.require(:task_status).permit(:name, :is_close)
  end
end
