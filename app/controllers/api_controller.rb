class ApiController < ApplicationController
  
  skip_before_action :authenticate_user!, :only => [:reply]
  skip_before_action :user_permitted_functionality, :only => [:reply]


  
  def reply
    @comment = params[:comment]
    @id = params[:question_id]
    @secret = params[:s]
    @answer = 
      if params[:answer] == '1' 
        'Yes' 
      elsif params[:answer].blank?
        'No'
      else
        params[:answer]
      end
    
    url = "http://esbsrv01.monopoly.su/Questions/Reply.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'Id':'#{@id}', 'Answer':'#{@answer}', 'Secret':'#{@secret}', 'Comment':'#{@comment}'}"
    response = http.request(request)
  
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  def reply_open
    @id = params[:question_id]
    @secret = params[:s]
    @answer = params[:answer]
    
    url = "http://esbsrv01.monopoly.su/Questions/OpenQuestions/Reply.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'Id':'#{@id}', 'Answer':'#{@answer}', 'Secret':'#{@secret}', 'Comment':'#{@comment}'}"
    response = http.request(request)
  
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  def prm_questions 
    if Rails.env == 'development'
      @prm_questions = get_prm_questions 
    else  
      @prm_questions = get_prm_questions current_user.email[0..current_user.email.index('@')-1]
    end  
    #@prm_questions.sort_by! { |q| [q['Priority'].blank? ? 0 : q['Priority'], q['Priority']] }.reverse unless @prm_questions.blank?
  end
  
  def show_prm_question
    @question = get_prm_question(params[:id])#['Question']
    unless @question['Parent'].blank?
      @transport = AgreementRequest.where(id: @question['Parent']).limit(1).first
      @agreement_requests = AgreementRequest.find_by_transport_and_time(@transport.initiator_object_id, @question['ArchiveLength']) if @transport
    end  
  end
  
  def reply_prm
    list_value = []
    list_value = params[:list_value].split(',').delete_if { |l| l.blank?}.compact.uniq.to_s unless params[:list_value].blank?
    @resp = reply_prm_question params[:question_id], params[:s], params[:int_value], params[:date_time_value], params[:bool_value], params[:timer_value], params[:timer_moment], params[:comment_value], params[:radio_group_value], list_value
    GetQuestions.load(params[:question_id]) unless @resp['Result'] == 'Bad'
    if @error
      render :show_prm_question
    else
      prm_questions
      render :prm_questions
    end  
  end
  
  def questions
    url = "http://esbsrv01.monopoly.su/Questions/NewQuestions.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'Secret':12345, 'UserLogin':'#{user_login}'}"
    response = http.request(request)
    
    @content = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    @questions = @content['Questions'] + open_questions['Questions']
    @questions.sort_by! { |a| [ a['Subject'], a['Created'] ] }
    respond_to do |format|
      format.html {}
      format.js {}       
    end
  end
  
  def show_question
    @question = get_question(params[:id])["Question"] 
    unless @question['Parent'].blank?
      @transport = AgreementRequest.where(id: @question['Parent']).limit(1).first
      @agreement_requests = AgreementRequest.find_by_transport(@transport.initiator_object_id) if @transport
#        @agreement_requests = AgreementRequest.where(initiator_object_id: @transport.initiator_object_id)
    end  
#    render text: AgreementRequest.where(initiator_object_id: @transport.initiator_object_id).inspect
#    render text:@transport.initiator_object_id
  end
  
  def send_answer
    reply
    GetQuestions.load(@id) unless @resp['Result'] == 'Bad'
    questions
  end
  
  def send_open_answer
    reply_open
    GetQuestions.load(@id) unless @resp['Result'] == 'Bad'
    questions
    render :send_answer
  end
  
  def show_add
    @truck_drivers = Refill.get_truck_drivers
  end
  
  def get_azs
    @transport_key = params[:transport_key]
    @azs = Refill.get_azs(@transport_key)
    @info = Refill.get_info(@transport_key).first['body']
    @agreement_requests = AgreementRequest.find_by_transport(@transport_key)
  end
  
  def set_azs
    answer = 1 #params[:answer].blank? ? 0 : 1
    Refill.set_azs(params[:transport_key], params[:destination], answer, params[:comment], current_user.email[0..current_user.email.index('@')-1])
  end
  
  def lock_question
    LockedQuestion.create(question_id: params[:id], user_id: current_user.id) unless params[:id].blank?
  rescue
  ensure  
    render nothing: true
  end
  
  def unlock_question
    LockedQuestion.where(question_id: params[:id]).delete_all unless params[:id].blank?
  rescue
  ensure  
    render nothing: true
  end
  
  
  private
  
  def get_prm_questions user_login = nil
    url = "http://esbsrv01.monopoly.su/Questions/ParametrizedQuestions/NewQuestions.ashx"
    user_login_str = "'UserLogin':'#{user_login}'," if user_login
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{#{user_login_str if user_login_str} 'Secret':'12345'}"
    response = http.request(request)
    get_esb_result_new response
  end
  
  def reply_prm_question id, secret, int_value , date_time_value, bool_value, timer_value, timer_moment, comment_value, radio_group_value, list_value
    url = "http://esbsrv01.monopoly.su/Questions/ParametrizedQuestions/Reply.ashx"
    bool_value_str = 
      if bool_value.blank?
        nil
      elsif bool_value == '0'
        "'BoolValue': false,"
      elsif bool_value == '1'
        "'BoolValue': true,"
      end
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{'Id':'#{id}', 'Secret':'#{secret}', 'IntValue':'#{int_value}', 'DateTimeValue':'#{date_time_value.to_datetime.to_s(:db) unless date_time_value.blank?}', #{bool_value_str if bool_value_str}
                    'TimerValue':'#{timer_value}', 'TimerMoment':'#{timer_moment.to_datetime.to_s(:db) unless timer_moment.blank?}', 'CommentValue':'#{comment_value}', 'RadioGroupValue':'#{radio_group_value}', 
                    'ListValue':#{list_value}}"
    response = http.request(request)
    body = request.body
    resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if resp['Result'] == 'Bad'
      @error = resp.to_s + ' : ' + body
    end
    resp
    #get_esb_result_new response
  end
  
  def get_prm_question id
    #e96864ad-3775-4339-81f5-e295f6ad89ff
    url = "http://esbsrv01.monopoly.su/Questions/ParametrizedQuestions/GetQuestion.ashx?id=#{id}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result_new response
#    resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8)) 
  end
  
  def get_question(id)
    req = question_request "http://Esbsrv01.monopoly.su/Questions/OpenQuestions/OpenQuestionJson.ashx", id   
    unless req["Ok"]
      req = question_request "http://Esbsrv01.monopoly.su/Questions/QuestionJson.ashx", id
    end
    req
  end
  
  def question_request(url, id)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'Secret':12345, 'Id':'#{id.downcase}'}"
    response = http.request(request)
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  
  def user_login
    current_user.email[0..current_user.email.index('@')-1]
  end
  
  def open_questions
    url = "http://esbsrv01.monopoly.su/Questions/OpenQuestions/NewOpenQuestions.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body =  "{'Secret':12345, 'UserLogin':'#{user_login}'}"
    response = http.request(request)
    
    JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
  class GetQuestions < Monosql
    def self.load(id)
      connection.select_all("exec [MonopolySun].[dbo].[ms_mst_question_answer_GetResult] @foreign_id = '#{id}'").to_a
    end
  end
  
  
  class Refill < Monosql  
    def self.get_truck_drivers
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_TruckDrivers_GetFromST]").to_a
    end
    
    def self.get_azs(transport_key)
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_AZS_GetFromMS] @TransportKey = '#{transport_key}'").to_a
    end
    
    def self.set_azs(transport_key, destination, answer, comment, dn)
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_AZS_SetForMS] @TransportKey = '#{transport_key}', @DestinationKey = '#{destination}', @IsOk = #{answer}, @Comment = '#{comment}', @dn = '#{dn}'").to_a
    end
    
    def self.get_info(transport_key)
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_AZSBody_GetFromMS] @TransportKey = '#{transport_key}'").to_a
    end
  end
  
  
  
end
