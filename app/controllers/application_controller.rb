class ApplicationController < ActionController::Base
  #force_ssl if: :ssl_configured?
  protect_from_forgery
  require 'will_paginate/array'

  
  before_action :authenticate_user!
  before_action :user_permitted_functionality, unless: :skip_for_admin?

  after_filter :user_activity

  
  layout :layout_by_resource

  def layout_by_resource
    if devise_controller? && resource_name == :user && action_name == 'new'
      "devise/sessions"
    else
      "application"
    end
  end
  

  def ssl_configured?
    !Rails.env.development?
  end
  
  
 # rescue_from ActiveRecord::RecordNotFound, :with => :not_found

  def referrer_controller
    ref = request.referrer
    rec = ref.split('/')[3]
    rec
  end
  
    
  def get_regions destination = nil
    unless destination.blank?
      url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetRegions.ashx?DestinationKey=#{destination}"
    else  
      url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetRegions.ashx"
    end
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end

 
  def get_esb_result data
    resp = JSON.parse(data.body.to_s.force_encoding(Encoding::UTF_8))
    if resp['Ok'] 
      resp['Data'] 
    else
      @error = resp['ErrorMessage']
      @error += resp['TechInfo'] if Rails.env == 'development' && resp['TechInfo']
      nil
    end
  end
 
  def get_esb_result_new data
    resp = JSON.parse(data.body.to_s.force_encoding(Encoding::UTF_8))
    if resp['Result'] == 'Ok' 
      resp['Data']
    else
      nil
    end
  end
  
  
  def controllers_actions
    routes = Rails.application.routes.routes.map{ |route| {controller: route.defaults[:controller], action: route.defaults[:action]} }.compact
    @routes = {}
    routes.each{|m| @routes[m[:controller]] = [] unless m[:controller].nil? }
    routes.each{|m| @routes[m[:controller]] += [m[:action]] unless m[:controller].nil? }
    @ctrl = @routes.keys
    @act = []
    @routes.each { |k, v| v.each { |a| @act << [k, a] unless ['update', 'create', 'destroy'].include?(a)  } }
    @act.uniq!.sort!
  end  


  private

# def not_found
#     respond_with(nil, :status => 404) do |format|
#      format.html {
#         flash[:error] = I18n.t(:error404, :scope => "system")
#        redirect_to(root_path)
#       }
#      format.xml { render :text => {'error' => 'Resource not found'}.to_xml(:root => 'errors'), :status => 404 }
#      format.js { render :text => {'error' => 'Resource not found'}, :status => 404 }
#      format.json { render :text => {'errors' => ['Resource not found']}.to_json, :status => 404 }
#     end
# end
 
  
  
  def skip_for_admin? 
    user_signed_in? && current_user.admin?
  end
  
  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(users)
    root_path
  end
  def after_sign_in_path_for(users)
    root_path
  end

#def after_sign_in_path_for(users)
#  request.referrer
#end

  def user_permitted_functionality # NEED REFACTORING !!!!!!!!!
    if user_signed_in? && !request.xhr? && request.get?
      current_user_func = current_user.all_f_groups.map(&:name).compact.uniq
      current_user_func_controller = current_user.all_f_groups_descendents.map(&:controller).compact.uniq
      current_url = request.path
      current_controller = params[:controller]
      current_url.gsub("/index", '')
      fg = Functionalgroup.where(link: current_url).map(&:root).map(&:name)
      fg_controller = Functionalgroup.where(controller: current_controller).map(&:controller).compact.uniq
      
      if fg.any?{ |f| current_user_func.include?(f) } || fg_controller.any?{ |f| current_user_func_controller.include?(f) }
        true
      else
        redirect_to root_url
      end
    end  
  end


  def user_activity
    current_user.try :touch #if Rails.env == 'production'
  end


protected

  def get_table_data(str)
    url = str 
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    resp = http.request(request)
    JSON.parse(resp.body.to_s.force_encoding(Encoding::UTF_8))
  end
  
end
