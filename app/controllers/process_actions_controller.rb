class ProcessActionsController < ApplicationController
  before_action :set_process_action, only: [:show, :edit, :update, :destroy]


  def index
    @process_actions = ProcessAction.all
  end

  def show
  end

  def new
    @process_action = ProcessAction.new
  end

  def edit
  end

  def create
    @process_action = ProcessAction.new(process_action_params)

    if @process_action.save
      redirect_to process_actions_url, notice: 'Действие по процессу добавлено'
    else
      render :new
    end
  end

  def update
    if @process_action.update(process_action_params)
      redirect_to process_actions_url, notice: 'Действие по процессу сохранено'
    else
      render :edit
    end
  end

  def destroy
    @process_action.destroy
    redirect_to process_actions_url, notice: 'Действие по процессу удалено'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_process_action
    @process_action = ProcessAction.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def process_action_params
    params.require(:process_action).permit(:name, :descr, :is_close)
  end
end
