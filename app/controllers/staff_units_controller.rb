# -*- encoding : utf-8 -*-

class StaffUnitsController < ApplicationController


  def index
    #@contractors = Contractor.find(Department.select('distinct contractor_id').map(&:contractor_id))
    @contractors = Contractor.includes(:departments)
    @departments = Department.visible.order(:name)
    
    @staff_units_count = StaffUnit.count

    unless params[:contractor].blank?
      @contractor = Contractor.find(params[:contractor])
      unless params[:department].blank?
        @department = Department.find(params[:department])
        @staff_units = @department.staff_units.sort { |a, b| a.department <=> b.department }#.paginate(page: params[:page], per_page: 20)
      else
        @staff_units = @contractor.staff_units.sort { |a, b| a.department <=> b.department }#.paginate(page: params[:page], per_page: 20)
      end
    else
      @staff_units = StaffUnit.joins(:department).order('departments.name, staff_units.position_id ')#.paginate(page: params[:page], per_page: 20)
    end



    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @staff_units }
    end
  end


  def show
    @staff_unit = StaffUnit.find(params[:id])

    respond_to do |format|
      format.js {}
    end
    
  end


  def new
    @staff_unit = StaffUnit.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @staff_unit }
    end
  end


  def edit
    @staff_unit = StaffUnit.find(params[:id])
  end


  def create
    @staff_unit = StaffUnit.new(staff_unit_params)

    respond_to do |format|
      if @staff_unit.save
        format.html { redirect_to staff_units_url, notice: 'Штатная единица добавлена' }
        format.json { render json: @staff_unit, status: :created, location: @staff_unit }
      else
        format.html { render action: "new" }
        format.json { render json: @staff_unit.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @staff_unit = StaffUnit.find(params[:id])

    respond_to do |format|
      if @staff_unit.update_attributes(staff_unit_params)
        format.html { redirect_to staff_units_url, notice: 'Штатная единица сохранена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @staff_unit.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @staff_unit = StaffUnit.find(params[:id])
    @staff_unit.close

    respond_to do |format|
      format.html { redirect_to staff_units_url }
      format.json { head :no_content }
    end
  end


 private

 def staff_unit_params
   params[:staff_unit].each do |k,v|
       params[:staff_unit][k.to_sym] = nil if params[:staff_unit][k.to_sym].blank? and  k!= 'is_close'
   end
   params.require(:staff_unit).permit(:department_id, :descr, :head, :id, :position_id, :is_close, :rgt, :lft, :parent_id)
 end


end

