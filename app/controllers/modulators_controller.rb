class ModulatorsController < ApplicationController
  before_action :set_modulator, only: [:edit, :update, :destroy]
  before_action :get_contractors, only: [:edit, :new, :index]

  respond_to :html, :js

  def index
    @modulators = Modulator.all#includes(:contractor)
    @modulators.each do |m| 
      price = @contractors.find { |c| c.first.downcase == m.contractor_id.downcase }
      m.price = price.last.to_f.round 2 if price
      m.mileage = (m.traffic / price.last.to_f).round 2 if price
    end
    @from = user_session[:modulator_from] || 3.days.ago.to_date
    @to = user_session[:modulator_to] || Time.now.to_date

    @total_price = @modulators.map { |m| m.price }.compact.sum || 0
    @total_mileage = @modulators.map { |m| m.mileage }.compact.sum || 0
    @total_traffic = @modulators.map { |m| m.traffic }.compact.sum || 0
    
    respond_with(@modulators)
  end

  def set_date
    user_session[:modulator_from] = params[:from] || 3.days.ago.to_date
    user_session[:modulator_to] = params[:to] || Time.now.to_date
    redirect_to modulators_url
  end
  
  
  def new
    @modulator = Modulator.new
    respond_with(@modulator)
  end

  def edit
    render :new
  end

  def create
    @modulator = Modulator.new(modulator_params)
    respond_to do |format|
      format.html do 
        if @modulator.save
          redirect_to modulators_url, notice: 'Модулятор сохранен' 
        else
          render :new
        end  
      end
    end  
  end

  def update
    respond_to do |format|
      format.html do 
        if @modulator.update(modulator_params)
          redirect_to modulators_url, notice: 'Модулятор сохранен' 
        else
          render :new
        end  
      end
    end 
  end

  def destroy
    @modulator.close
    redirect_to modulators_url
  end

  private
    def get_contractors
      user_session[:modulator_from] ||= 3.days.ago.to_date
      user_session[:modulator_to] ||= Time.now.to_date
      @contractors = Modulator.get_contractors(user_session[:modulator_from].to_time.to_i, user_session[:modulator_to].to_time.to_i)
    end
  
    def set_modulator
      @modulator = Modulator.find(params[:id])
    end

    def modulator_params
      params.require(:modulator).permit(:contractor_id, :traffic, :is_close)
    end
    
end
