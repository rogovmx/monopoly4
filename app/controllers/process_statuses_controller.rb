class ProcessStatusesController < ApplicationController
  before_action :set_process_status, only: [:show, :edit, :update, :destroy]


  def index
    @process_statuses = ProcessStatus.all
  end

  def show
  end

  def new
    @process_status = ProcessStatus.new
  end

  def edit
  end

  def create
    @process_status = ProcessStatus.new(process_status_params)

    if @process_status.save
      redirect_to process_statuses_url, notice: 'Статус процесса добавлен'
    else
      render :new
    end
  end

  def update
    if @process_status.update(process_status_params)
      redirect_to @process_status, notice: 'Статус процесса сохранен'
    else
      render :edit
    end
  end

  def destroy
    @process_status.destroy
    redirect_to process_statuses_url, notice: 'Статус процесса удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_process_status
    @process_status = ProcessStatus.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def process_status_params
    params.require(:process_status).permit(:name, :is_close)
  end
end
