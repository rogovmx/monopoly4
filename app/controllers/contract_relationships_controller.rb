# -*- encoding : utf-8 -*-


class ContractRelationshipsController < ApplicationController


  def index
 #   @contract_relationships = ContractRelationship.all
    @contract_relationships_count = ContractRelationship.count
    @contract_relationships = ContractRelationship.all#.paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contract_relationships }
    end
  end


  def show
    @contract_relationship = ContractRelationship.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract_relationship }
    end
  end


  def new
    @contract_relationship = ContractRelationship.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract_relationship }
    end
  end


  def edit
    @contract_relationship = ContractRelationship.find(params[:id])
  end


  def create
    @contract_relationship = ContractRelationship.new(contract_relationship_params)

    respond_to do |format|
      if @contract_relationship.save
        format.html { redirect_to contract_relationships_url, notice: 'Отношение сторон добавлено' }
        format.json { render json: @contract_relationship, status: :created, location: @contract_relationship }
      else
        format.html { render action: "new" }
        format.json { render json: @contract_relationship.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract_relationship = ContractRelationship.find(params[:id])

    respond_to do |format|
      if @contract_relationship.update_attributes(contract_relationship_params)
        format.html { redirect_to contract_relationships_url, notice: 'Отношение сторон сохранено' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract_relationship.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract_relationship = ContractRelationship.find(params[:id])
    @contract_relationship.close
    #@contract_relationship.destroy

    respond_to do |format|
      format.html { redirect_to contract_relationships_url }
      format.json { head :no_content }
    end
  end


 private

 def contract_relationship_params
   params.require(:contract_relationship).permit(:contract_id, :contract_kind_role_id, :contractor_id, :id, :is_close)
 end


end

