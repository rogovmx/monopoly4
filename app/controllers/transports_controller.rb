class TransportsController < ApplicationController

  def groupings
    user_session[:transport_groupings] = params[:vis] unless params[:vis].blank?
    
    @trucks = 
    case user_session[:transport_groupings]
    when 'without_trailer'
      Transport.free_trucks
    when 'without_driver'
      Transport.without_driver
    else
      Transport.trucks.includes(:chained_coupler_trucks, :chained_truck_drivers, chained_coupler_trucks: :trailer, chained_truck_drivers: [employee: :person])
    end
    
    
  end
  
  def show_couplers_and_drivers
    @truck = Transport.find(params[:id])
    @couplers = @truck.coupler_trucks.order("created_at desc")
    @truck_drivers = @truck.truck_drivers.order("created_at desc")
    respond_to do |format|
      format.js {  }
    end
  end
  
  #--------------- COUPLERS ---------------------
  
  def new_and_edit_coupler
    @truck = Transport.find(params[:id])
    @free_trailers = Transport.free_trailers
    @coupler = params[:coupler].blank? ? Coupler.new : Coupler.find(params[:coupler])
    @coupler.truck_id = @truck.id
    respond_to do |format|
      format.js {  }
    end
  end
  
  def create_and_update_coupler
    @coupler = params[:coupler][:id].blank? ? Coupler.new : Coupler.find(params[:coupler][:id])
    @coupler.update(coupler_params)
    @truck = Transport.find(@coupler.truck_id)
    respond_to do |format|
      if @coupler.save
        format.js do 
          groupings
          @couplers = @truck.coupler_trucks.order("created_at desc")
          @truck_drivers = @truck.truck_drivers.order("created_at desc")
          flash[:notice] = 'Сцепка сохранена' 
          render :groupings
        end
      else
        format.js do 
          @free_trailers = Transport.free_trailers
          render :new_and_edit_coupler
        end
      end
    end
  end

  
  def unchain_coupler
    @coupler = Coupler.find(params[:id])
    @coupler.close_coupler
    groupings 
    @truck = @coupler.truck
    @couplers = @truck.coupler_trucks.order("created_at desc")
    @truck_drivers = @truck.truck_drivers.order("created_at desc")

    respond_to do |format|
      format.js do 
        flash[:notice] = 'Сцепка расцеплена'
        render :groupings
      end
    end
    
  end
  
  #--------------- END COUPLERS --------------------
  
  #--------------- TRUCK_DRIVERS ---------------------
  
  def new_and_edit_driver
    @truck = Transport.find(params[:id])
    @free_drivers = Employee.free_drivers
    @truck_driver = params[:truck_driver].blank? ? TruckDriver.new : TruckDriver.find(params[:truck_driver])
    @truck_driver.transport_id = @truck.id
    respond_to do |format|
      format.js {  }
    end
  end
  
  def create_and_update_driver
    @truck_driver = params[:truck_driver][:id].blank? ? TruckDriver.new : TruckDriver.find(params[:truck_driver][:id])
    @truck_driver.update(truck_driver_params)
    @truck = Transport.find(@truck_driver.transport_id)
    respond_to do |format|
      if @truck_driver.save
        format.js do 
          groupings
          @couplers = @truck.coupler_trucks.order("created_at desc")
          @truck_drivers = @truck.truck_drivers.order("created_at desc")
          flash[:notice] = 'Связка сохранена' 
          render :groupings
        end
      else
        format.js do 
          @free_drivers = Employee.free_drivers
          render :new_and_edit_driver
        end
      end
    end
  end

  
  def unchain_driver
    @truck_driver = TruckDriver.find(params[:id])
    @truck_driver.unchain_truck_driver
    groupings 
    @truck = Transport.find(@truck_driver.transport_id)
    @couplers = @truck.coupler_trucks.order("created_at desc")
    @truck_drivers = @truck.truck_drivers.order("created_at desc")

    respond_to do |format|
      format.js do 
        flash[:notice] = 'Водитель отвязан'
        render :groupings
      end
    end
    
  end
  
  #--------------- END TRUCK_DRIVERS --------------------
  

  def index
    if params[:showdel] == "true"
      @transports = Transport.all.includes(:contractor, :documents, :transportmodel, transportmodel: :transportbrand)
    else
      @transports = Transport.visible.includes(:contractor, :documents, :transportmodel, transportmodel: :transportbrand)
    end

    respond_to do |format|
      format.html 
    end
  end
  
  def new
    @transport = Transport.new
    @brands = Transportbrand.all
    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transport = Transport.find(params[:id])
    @brands = Transportbrand.all
    if params[:copy]
      @copy = Marshal.load(Marshal.dump(@transport.attributes))
      @copy.delete('id')
      @copy.delete('created_at')
      @copy.delete('updated_at')
      @transport_copy = Transport.create(@copy)
      @transport_copy.enginemodels << @transport.enginemodel if @transport.enginemodel
      @transport = @transport_copy
    end
    transport_stats(@transport)
    
    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def create
    @transport = Transport.new(transport_params)
    @brands = Transportbrand.all
    @models = Transportmodel.all

    respond_to do |format|
      if @transport.save
        format.html { redirect_to transports_url, notice: 'ТС сохранено.' }

      else
       # format.js   {}
        transport_stats(@transport)
        flash[:notice] = "Ошибка сохранения"
        format.html { render action: "new" }

      end
      #render action: "update"
    end
  end


  def update
    @transport = Transport.find(params[:id])
    @brands = Transportbrand.all
    @models = Transportmodel.all
    respond_to do |format|
      if @transport.update_attributes(transport_params)
         transport_stats(@transport)
         flash[:notice] = "Ok"
         format.html { redirect_to transports_url, notice: 'ТС сохранено.' }
#        format.json { head :no_content }
      else
        format.js   {}
        transport_stats(@transport)
         flash[:notice] = "Error"
        format.html { render action: "edit" }
#        format.json { render json: @transport.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @transport = Transport.find(params[:id])
    current_user.admin ? @transport.destroy : @transport.close

    respond_to do |format|
      format.html { redirect_to transports_url }
    end
  end

  def remove_doc
    @document = Document.find(params[:id])
    tmp = @document
    tmp_tr = tmp.transport
    current_user.admin ? @document.destroy : @document.close

    respond_to do |format|
      format.html { redirect_to transports_url, notice: "Документ #{tmp.documentkind.name} для #{tmp_tr.reg_num} удален" }
    end
  end




  def transport_stats(transport)
    @transportmodel = transport.transportmodel if transport.transportmodel #модель ТС
    if @transportmodel
      @transportmodel_name = @transportmodel.name
      @transportbrand = @transportmodel.transportbrand.name # марка ТС
      @transportengine = @transportmodel.enginemodel if @transportmodel.is_engine #наличие двигателя
      @transportloadings = (transport.tsloadings.any? ? transport.tsloadings : @transportmodel.transportloadings) if @transportmodel.is_loading #наличие погрузки
      @transportfuellings = (transport.tstanks.any? ? transport.tstanks : @transportmodel.transporttanks) if @transportmodel.is_fuelling #наличие топл. бака
      @transportkind = @transportmodel.transportkind.name if @transportmodel.transportkind #Вид ТС
      @transporttype = @transportmodel.transportkind.transporttype.name if @transportkind # Тип ТС
      @transportcategory = @transportmodel.transportkind.transporttype.transportcategory.name if @transporttype # Категория ТС
      @enginetype = @transportmodel.enginemodel.enginetype.name if @transportmodel.enginemodel && @transportmodel.enginemodel.enginetype
      @enginemodel = @transportmodel.enginemodel.name if @transportmodel.enginemodel 
    end
    @transportexploitation = transport.transportexploitation.name if transport.transportexploitation
    #@enginemodel = transport.enginemodel if transport.enginemodel #модель двигателя
#    if @enginemodel
#      @enginemodel_name = @enginemodel.name
#      @enginemodel_power = @enginemodel.power
#      @enginemodel_power_kvt = @enginemodel.power_kvt
#      @enginemodel_displacement = @enginemodel.displacement
#    end
    @transportcolor = transport.transportcolor.name if transport.transportcolor #цвет тс
  end


  def get_transportkind
    unless params[:transportmodel].blank?
      @transportmodel = Transportmodel.find(params[:transportmodel]) 
      @transportkind = @transportmodel.transportkind.name if @transportmodel.transportkind #Вид ТС
      @transporttype = @transportmodel.transportkind.transporttype.name if @transportkind # Тип ТС
      @transportcategory = @transportmodel.transportkind.transporttype.transportcategory.name if @transporttype # Категория ТС

      @transportengine = @transportmodel.enginemodel if @transportmodel.is_engine #наличие двигателя
      @enginemodel = @transportmodel.enginemodel.name if @transportmodel.enginemodel 
      @enginetype = @transportmodel.enginemodel.enginetype.name if @transportmodel.enginemodel && @transportmodel.enginemodel.enginetype
      @transportloadings =  @transportmodel.transportloadings if @transportmodel.is_loading #наличие погрузки
      @transportfuellings =  @transportmodel.transporttanks if @transportmodel.is_fuelling #наличие топл. бака
    end  
    respond_to do |format|
      format.js   {}
    end
  end



  def select_enginemodel
   respond_to do |format|
      format.js   {}
   end
    @transport = Transport.find(params[:id])
    @enginemodel = Enginemodel.find(params[:enginemodel][:id])

    @transport.enginemodels.delete( @transport.enginemodels )
    @transport.enginemodels << @enginemodel
    transport_stats(@transport)
    flash[:notice] = "сохранена модель двигателя: #{@enginemodel.name}"
    render action: "complete"
  end

  def select_loading
    respond_to do |format|
      format.js   {}
    end
    @transport = Transport.find(params[:id])
    @transport.tsloadings.create(loading: params[:tsloading][:loading])
    @transport.reload
    transport_stats(@transport)
    flash[:notice] = 'Тип загрузки добавлен'
    render action: "complete"
  end

  def del_loading_from_transport
    @transport = Transport.find(params[:id])
    Tsloading.find(params[:loading]).delete
    transport_stats(@transport)
    flash[:notice] = 'Тип загрузки удален'
    render action: "complete"
  end

  def select_tank
    respond_to do |format|
      format.js   {}
    end
    @transport = Transport.find(params[:id])
    @transport.tstanks.create( v_tank: params[:tstank][:v_tank], v_tank_min: params[:tstank][:v_tank_min] )
    @transport.reload
    transport_stats(@transport)
    flash[:notice] = 'Топливный бак добавлен'
    render action: "complete"
  end

  def del_tank_from_transport
    @transport = Transport.find(params[:id])
    Tstank.find(params[:tank]).delete
    transport_stats(@transport)
    flash[:notice] = 'Топливный бак удален'
    render action: "complete"
  end



  def show_part
   respond_to do |format|
      format.js   {}
   end
   @part = params[:part]
   @transport = Transport.find(params[:id]) if params[:id]
  end


  def complete

  end


  private

  def transport_params
    params[:transport][:transportexploitation_id] = nil if params[:transport][:transportexploitation_id].blank?
    params[:transport][:transportcolor_id] = nil if params[:transport][:transportcolor_id].blank?
    params.require(:transport).permit(:description, :name, :vin, :body_num, :chassis_num, :engine_num, :year, :weight, :capacity, :reg_num, :is_close,
                  :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume,
                  :registration_date, :deregistration_date, :id, :contractor_id, :transportmodel_id, :transportexploitation_id, :transportcolor_id)
  end
 
  def coupler_params
    params.require(:coupler).permit(:end_date, :id, :start_date, :trailer_id, :truck_id, :is_close, :locked_to_date)
  end
 
  def truck_driver_params
    params.require(:truck_driver).permit(:employee_id, :end_date, :id, :is_close, :start_date, :transport_id)
  end
  
  
end
