class ContactsController < ApplicationController
  before_action :set_contact, only: [:edit, :update, :destroy]


  def index
    @contacts = Contact.includes(:person, :contact_type)
  end


  def new
    @contact = Contact.new
  end

  def edit
  end

  def create
    @contact = Contact.new(contact_params)
    respond_to do |format|
      format.html do
        @contact.save ? redirect_to( contacts_url, notice: 'Контакт добавлен') : render(:new)
      end  
      format.js do # save from people_controller
        @person = @contact.person
        @contacts = @person.contacts
        @contact = Contact.new if @contact.save
        render "people/contacts"
      end
    end

  end

  def update
    respond_to do |format|
      format.html do
        @contact.update(contact_params) ? redirect_to(contacts_url, notice: 'Контакт сохранен') : render(:edit)
      end  
      format.js do # update from people_controller
        @person = @contact.person
        @contacts = @person.contacts
        @contact = Contact.new if @contact.update(contact_params)
        render "people/contacts"
      end
    end
  end

  def destroy
    @contact.destroy
     respond_to do |format|
      format.html do
        redirect_to contacts_url, notice: 'Контакт удален'
      end  
      format.js do # del from people_controller
          @person = @contact.person
          @contacts = @person.contacts
          render "people/contacts"
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contact_params
      params.require(:contact).permit(:contact_type_id, :person_id, :name, :is_close)
    end
end
