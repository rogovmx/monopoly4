class SaveReportsXlsController < ApplicationController
  
  def index
    @title = JSON.parse(params[:title])
    @data = JSON.parse(params[:data])

    respond_to do |format|
      format.xls {
        connections = Spreadsheet::Workbook.new
        list = connections.create_worksheet name: 'Report'
        list.row(0).concat @title
        @data.each_with_index { |data, i| list.row(i+1).push *data }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        connections.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Отчет_#{Time.now.to_i.to_s}.xls"
        }
    end
  end

end
