# -*- encoding : utf-8 -*-
class SmsController < ApplicationController
  
  skip_before_action :authenticate_user!, :only => [:send_now, :api]
  
  
  def index
  end

def send_sms
  require 'digest/md5'
  require 'net/http'
  #require 'uri'

  pass = Digest::MD5.hexdigest('79119887257').to_s.strip
  @number = params[:number].strip
  @text = params[:text].strip

 url = "http://mcommunicator.ru/m2m/m2m_api.asmx/SendMessage"
 uri = URI.parse(url)
 http = Net::HTTP.new(uri.host, uri.port)
 request = Net::HTTP::Post.new(uri.request_uri)
 parameters =  {"msid" => "#{@number}", "message" => "#{@text}", "naming" => "79857707575", "login" => "79119887257", "password" => "#{pass}"}
 request.set_form_data(parameters)
 response = http.request(request)
 #puts response
 content = response.body

  pos1 = content.index('</long>')
  num = content[pos1-8..pos1-1] if pos1
  #@content = "#@number - #@text"
  flash[:notice] = "SMS отправлена"
  @content = num
end

def api
  require 'digest/md5'
  require 'net/http'
  #require 'uri'

  pass = Digest::MD5.hexdigest('79119887257').to_s.strip
  @number = params[:number].strip
  @text = params[:text].strip

 url = "http://mcommunicator.ru/m2m/m2m_api.asmx/SendMessage"
 uri = URI.parse(url)
 http = Net::HTTP.new(uri.host, uri.port)
 request = Net::HTTP::Post.new(uri.request_uri)
 parameters =  {"msid" => "#{@number}", "message" => "#{@text}", "naming" => "79857707575", "login" => "79119887257", "password" => "#{pass}"}
 request.set_form_data(parameters)
 response = http.request(request)
 #puts response
 content = response.body

  pos1 = content.index('</long>')
  num = content[pos1-8..pos1-1] if pos1
  render text: num
end

def send_now
 render text: send_sms
end  



 private

 def sms_params
   params.require(:sms).permit(:phone_number, :sms_text, :message_id)
 end

end
