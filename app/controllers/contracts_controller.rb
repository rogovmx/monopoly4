# -*- encoding : utf-8 -*-


class ContractsController < ApplicationController


  def index
 #   @contracts = Contract.all
    @contracts_count = Contract.count
    @contracts = Contract.all #paginate(page: params[:page], per_page: 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contracts }
    end
  end


  def show
    @contract = Contract.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract }
    end
  end


  def new
    @contract = Contract.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract }
    end
  end


  def edit
    @contract = Contract.find(params[:id])
  end


  def create
    @contract = Contract.new(contract_params)

    respond_to do |format|
      if @contract.save
        format.html { redirect_to contracts_url, notice: 'Договор добавлен.' }
        format.json { render json: @contract, status: :created, location: @contract }
      else
        format.html { render action: "new" }
        format.json { render json: @contract.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @contract = Contract.find(params[:id])

    respond_to do |format|
      if @contract.update_attributes(contract_params)
        format.html { redirect_to contracts_url, notice: 'Договор сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @contract = Contract.find(params[:id])
    @contract.close
    #@contract.destroy

    respond_to do |format|
      format.html { redirect_to contracts_url }
      format.json { head :no_content }
    end
  end

 private

 def contract_params
   params.require(:contract).permit(:contract_number, :contract_subkind_id, :end_date, :id, :is_close, :start_date)
 end
 
end

