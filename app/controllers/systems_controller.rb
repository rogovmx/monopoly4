# -*- encoding : utf-8 -*-

class SystemsController < ApplicationController


  def index
    @systems = System.all#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @systems }
    end
  end


  def show
    @system = System.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @system }
    end
  end


  def new
    @system = System.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @system }
    end
  end


  def edit
    @system = System.find(params[:id])
  end


  def create
    @system = System.new(system_params)

    respond_to do |format|
      if @system.save
        format.html { redirect_to systems_url, notice: 'System добавлен.' }
        format.json { render json: @system, status: :created, location: @system }
      else
        format.html { render action: "new" }
        format.json { render json: @system.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @system = System.find(params[:id])

    respond_to do |format|
      if @system.update_attributes(system_params)
        format.html { redirect_to systems_url, notice: 'System сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @system.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @system = System.find(params[:id])
    @system.destroy

    respond_to do |format|
      format.html { redirect_to systems_url }
      format.json { head :no_content }
    end
  end


 private

 def system_params
   params.require(:system).permit(:descr, :end, :id, :is_close, :name, :start)
 end

end

