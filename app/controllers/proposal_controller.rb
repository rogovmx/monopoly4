class ProposalController < ApplicationController
  include ProposalHelper
  
 # --------------------- отмены ---------------------------------------

  def cancels
    @resource_types = get_resource_types
    
    @id = params[:id] unless params[:id].blank?
    @cancel_type = params[:cancel_type]
    @from_cancels = true
    unless @id.blank?
      @cancels =
        case @cancel_type
        when '1'; get_entry_cancels
        when '2'; get_work_cancels
        end
      @cancel = @cancels.find { |r| r['Id'] == @id }   
    end 
    if request.post?
      @resource_type = params[:resource_type]
      @name = params[:name]
      case @cancel_type
      when '1'
        set_entry_cancel @id, @resource_type, @name
      when '2'
        set_work_cancel @id, @resource_type, @name
      end
      index
      render :index unless @error
    elsif request.delete?
      case @cancel_type
      when '1'
        set_entry_cancel @id, nil, nil, true
      when '2'
        set_work_cancel @id, nil, nil, true
      end
      index
      render :index unless @error
    end
    if request.get? || @error
      @title = "Причина отмены"
      @render = 'cancel_form'
      render :show_popup
    end  
  end  

 
  
 # ------------------ Выполнение работ --------------------------------
    
  def works
    @works = get_job_performs
  end
  
  def works_dt
    works
       
      
     
    @works.map! { |work| [work["Number"], work["PersonName"], work["CreatedAt"].try(:to_datetime).to_s, work["DepartmentName"], work["StatusName"], 
                  work["ResourceTypeName"], work["ResourceName"], work["JobRequestReasonName"],  "#{work["PlannedDateStart"].try(:to_datetime)}",
                "#{work["PlannedDateEnd"].try(:to_datetime)}",  "#{work["FactDateStart"].try(:to_datetime)}",  "#{work["FactDateEnd"].try(:to_datetime)}",
                work["InaccessibilityTypeName"], work["StateOfResourceExists"] ? "<span class='glyphicon glyphicon-ok-circle green_letter'></span>" : "", 
                work["CanelReasonName"].to_s, work["CancelAuthorName"].to_s, work["CanelReasonMoment"].try(:to_datetime).to_s,
                work["JobRequests"].try(:size), work['Permissions'][1] == '1' ? "<a href='/proposal/set_work?id=#{work["Id"]}' data-remote='true' class='glyphicon glyphicon-edit'></a>" : "", 
                work['Permissions'][0] == '1' && can_delete_works_and_entries? ? "<a href='/proposal/show_work_cancels?id=#{work["Id"]}' data-remote='true' class='glyphicon glyphicon-remove red'></a>" : ""] }
       
     resp =  <<-eos 
      {
        "draw": 1,
        "data": #{@works.to_json}
      }
      eos
    render text: resp
  end
     
    
  def show_work_cancels
    works
    @id = params[:id]
    @work = @works.find { |r| r['Id'] == @id } unless @id.blank?
    @title = "Причины отмены выполнения"
    @render = 'show_work_cancels'
    render :show_popup
  end
  
  
  def set_work
    @works = get_job_performs
    @resource_types = get_resource_types
    @resources_by_type = get_resources_by_type
    @resources_by_type.sort_by! { |r| r['Name']} unless @error
    @departments = get_departments current_user.id
    @departments.sort_by! { |r| r['Name']} unless @error
    @department_reasons = get_departments_reasons
    @department_reasons.sort_by! { |r| r['JobRequestReasonName']} unless @error
    @regions = get_regions
    @regions.sort_by!(&:second) unless @error

    @id = params[:id] unless params[:id].blank?
    unless @id.blank?
      @work = @works.find { |r| r['Id'] == @id } 
      @destination_from = @work['DestinationKeyStart']
      @destination_to = @work['DestinationKeyEnd']
      @region_from = get_regions(@destination_from).first.first
      @region_to = get_regions(@destination_to).first.first
    end  
    if request.post?
      @resource_type = params[:resource_type]
      @resource = params[:resource]
      @department = params[:department]
      @reason = params[:dep_reason]
      @destination_from = params[:destination_from]
      @destination_to = params[:destination_to]
      @pl_start_date = params[:pl_start_date] == '__.__.____ __:__' ? '' : params[:pl_start_date]
      @pl_end_date = params[:pl_end_date] == '__.__.____ __:__' ? '' : params[:pl_end_date]
      @f_start_date = params[:f_start_date] == '__.__.____ __:__' ? '' : params[:f_start_date]
      @f_end_date = params[:f_end_date] == '__.__.____ __:__' ? '' : params[:f_end_date]
      @requests = params[:request].keys unless params[:request].blank?
      set_job_perform @id, @resource_type, @resource, @department, @reason, @destination_from, @destination_to, @pl_start_date, @pl_end_date, 
                      @f_start_date, @f_end_date, @requests, false, nil, false
      @works = get_job_performs
      render :works  unless @error              
    elsif request.delete?
      set_job_perform @id, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, false, params[:cancel_reason_id], false   
      @works = get_job_performs
      render :works  unless @error
    end
    if request.get? || @error
      @title = "Выполнение работ"
      @render = 'work_form'
      render :show_popup
    end  
  end
  
  def job_requests_filtred
    @id = params[:work_id]
    unless params[:reason].blank? || params[:resource].blank? || params[:department].blank?
      @job_requests = get_job_requests(params[:resource], params[:department], params[:reason], @id)
      @job_requests.sort_by! { |r| r['Number']} unless @error
    end  
    unless @id.blank?
      works = get_job_performs
      work = works.find { |r| r['Id'] == @id } 
      @current_requests = work['JobRequests'].map { |w| w['JobRequestId'] }
    end 
  end
     
  def work_locations
    @destinations_from = get_destinations_by_region params[:region_from] if params[:region_from]
    @destinations_to = get_destinations_by_region params[:region_to] if params[:region_to]
    @id = params[:work_id]
    unless @id.blank?
      works = get_job_performs
      work = works.find { |r| r['Id'] == @id } 
      @destination_from = work['DestinationKeyStart']
      @destination_to = work['DestinationKeyEnd']
    end 
    unless @error
      @destinations_from.sort_by!(&:second) if @destinations_from
      @destinations_to.sort_by!(&:second) if @destinations_to
    end  
  end
    
    # ------------------ Заявки --------------------------------
  def entries
    @job_requests = get_job_requests
  end
  
  def add_city_to_entry
    @city_key = params[:city_key]
    @city_name = params[:city_name]
  end
  
  def show_entry_cancels
    entries
    @id = params[:id]
    @job_request = @job_requests.find { |r| r['Id'] == @id } unless @id.blank?
    @title = "Причины отмены заявки"
    @render = 'show_entry_cancels'
    render :show_popup
  end

  
  def set_entry
    @id = params[:id] unless params[:id].blank?
    @job_requests = get_job_requests
    @job_request = @job_requests.find { |r| r['Id'] == @id } unless @id.blank?
    if request.post?
      @resource_type = params[:resource_type]
      @resource = params[:resource]
      @department = params[:department]
      @dep_reason = params[:dep_reason]
      @descr = params[:descr]
      @start_date = params[:start_date].blank? ? nil : params[:start_date].to_date.to_s(:db)
      @end_date = params[:end_date].blank? ? nil : params[:end_date].to_date.to_s(:db)
      @cities = params[:city].keys unless params[:city].blank?
      set_job_requests(@id, @dep_reason, @resource_type, @resource, @department, @descr, @start_date, @end_date, @cities.to_s)
      @job_requests = get_job_requests
      render :entries  unless @error
    elsif request.delete?
      set_job_requests(@id, nil, nil, nil, nil, nil, nil, nil, nil, params[:cancel_reason_id], false)
      @job_requests = get_job_requests
      render :entries  unless @error
    end
    @resource_types = get_resource_types
    @resources_by_type = get_resources_by_type
    @resources_by_type.sort_by! { |r| r['Name']} unless @error
    @departments = get_departments current_user.id
    @departments.sort_by! { |r| r['Name']} unless @error
    @department_reasons = get_departments_reasons
    @department_reasons.sort_by! { |r| r['JobRequestReasonName']} unless @error
    @cities_with_regions = get_cities_with_regions
    @cities_with_regions.map! { |c| [c.first, "#{c.second} (#{c.third})"] }.uniq! { |c| c.second }.sort_by! { |c| c.second} unless @error  
    if request.get? || @error
      @title = "Заявка на выполнение работ"
      @render = 'entry_form'
      render :show_popup
    end  
  end
  
  # ------------------ Причины и доступ к причинам --------------------------------
  def index
    @inaccess_types = get_inaccessibility_types
    @resource_types = get_resource_types
    @reasons = get_job_request_reasons
    @departments = get_departments current_user.id
    @departments.sort_by! { |r| r['Name']} unless @error
    @department_reasons = get_departments_reasons
    @entry_cancels = get_entry_cancels
    @work_cancels = get_work_cancels
  end
  
  
  def set_reason
#    render text: params[:entry_cancel]#.keys
    @entry_cancels = get_entry_cancels
    @work_cancels = get_work_cancels
    @from_set_reason = true
    @departments = get_departments current_user.id
    @reasons = get_job_request_reasons
    @inaccess_types = get_inaccessibility_types
    @resource_types = get_resource_types
    @id = params[:id] unless params[:id].blank?
    @reason = @reasons.find { |r| r['Id'] == @id } unless @id.blank?  
    @department_reasons = get_departments_reasons
    if request.post?  
      @entry_cancels_selected = params[:entry_cancel].keys unless params[:entry_cancel].blank?
      @work_cancels_selected = params[:work_cancel].keys unless params[:work_cancel].blank?
      set_job_request_reasons @id, params[:resource_type], params[:reason], params[:is_blocking], params[:inaccess_type], @entry_cancels_selected.to_s, @work_cancels_selected.to_s, false
      @reasons = get_job_request_reasons
      render :index unless @error
    elsif request.delete?  
      set_job_request_reasons @id, nil, nil, nil, nil, nil, nil, true
      @reasons = get_job_request_reasons
      render :index unless @error
    end
    if request.get? || @error
      @title = "Причина обращения"
      @render = 'reason_form'
      render :show_popup    
    end  
  end
  
  def add_cancel_to_reason
    unless params[:entry_cancel_select].blank? 
      @entry_cancels = get_entry_cancels
      @entry_cancel = @entry_cancels.find{ |c| c['Id'] == params[:entry_cancel_select] }
    end
    unless params[:work_cancel_select].blank? 
      @work_cancels = get_work_cancels
      @work_cancel = @work_cancels.find{ |c| c['Id'] == params[:work_cancel_select] }
    end
  end
  
  
  def set_deps_reason
    @entry_cancels = get_entry_cancels
    @work_cancels = get_work_cancels
    @departments = get_departments
    @reasons = get_job_request_reasons
    @inaccess_types = get_inaccessibility_types
    @resource_types = get_resource_types    
    @department_reasons = get_departments_reasons
    @id = params[:id] unless params[:id].blank?
    @department_reason = @department_reasons.find { |r| r['Id'] == @id } unless @id.blank?  
    if request.post?  
      set_departments_reasons @id, params[:reason], params[:department], false
      @department_reasons = get_departments_reasons
      render :index unless @error
    elsif request.delete?  
      set_departments_reasons @id, nil, nil, true
      @department_reasons = get_departments_reasons
      render :index unless @error
    end
    @reasons = get_job_request_reasons
    if request.get? || @error    
      @title = "Доступ к причинам обращений"
      @render = 'deps_reason_form'
      render :show_popup   
    end  
  end
  

  
  private
  
  # Сервис возврата типов недоступностей из SmartTruck
  def get_inaccessibility_types
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetInaccessibilityTypes.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  # Сервис возврата типов ресурсов
  def get_resource_types
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetResourceTypes.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  # Сервис возврата перечня причин обращения (с возможным указанием инициатора)
  def get_job_request_reasons
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetJobRequestReasons.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end

  #Сервис записи  причины обращения
  def set_job_request_reasons id, resource_type, name, is_blocking, inaccessibility_type, entry_cancel, work_cancel, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?
    entry_cancels_str = "'JobRequestCancelReasons':#{entry_cancel}," unless entry_cancel.blank?  
    work_cancels_str = "'JobPerformCancelReasons':#{work_cancel}," unless work_cancel.blank?  
    inaccessibility_str = "'STInaccessibilityTypeKey':'#{inaccessibility_type}'," unless inaccessibility_type.blank?
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetJobRequestReason.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    if is_close
      request.body = "{#{id_str if id_str} 'IsClose': #{is_close}}"
    else  
      request.body = "{#{id_str if id_str} 'ResourceTypeId':'#{resource_type}', 'Name':'#{name}', 
                      #{entry_cancels_str if entry_cancels_str} #{work_cancels_str if work_cancels_str}
                       'IsResourceBlocking': #{!is_blocking.blank?}, #{inaccessibility_str if inaccessibility_str} 'IsClose': #{is_close}}"
    end  
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp['JobRequestReasonId']
    else  
      @error = @resp['ErrorMessage']
      @error += request.body if Rails.env == 'development'
    end
  end

  # ==============================================================================
  
  
  # Сервис возврата подразделений
  def get_departments user = nil
    user_str = "?UserId=#{user}" if user
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetDepartments.ashx#{user_str if user_str}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  # Сервис возврата доступов к причине обращения
  def get_departments_reasons
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetDepartmentsJobRequestReasons.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  
  #Сервис записи доступа к причине обращенияя
  def set_departments_reasons id , job_request_reason, department, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetDepartmentJobRequestReason.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    if is_close
      request.body = "{#{id_str if id_str} 'IsClose': #{is_close}}"
    else  
      request.body = "{#{id_str if id_str} 'JobRequestReasonId':'#{job_request_reason}', 'DepartmentId':'#{department}', 'IsClose': #{is_close}}"
    end  
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp['JobRequestReasonId']
    else  
      @error = @resp['ErrorMessage']
      @error += request.body if Rails.env == 'development'
    end
  end
  
    # =================ENTRIES======================================================

  
  def get_job_requests resource = nil, department = nil, reason = nil, perform_id = nil
    if resource && reason && department
      url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetJobRequests.ashx?ResourceId=#{resource}&DepartmentId=#{department}&JobRequestReasonId=#{reason}#{('&JobPerformId=' + perform_id) unless perform_id.blank?}"
    else
      url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetJobRequests.ashx"
    end
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response 
  end
  
  def set_job_requests id, job_request_reason, resource_type, resource, department, descr, start_date, end_date, city_keys, cancel = nil, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?  
    city_keys_str = "'CitiesKeys':#{city_keys}," unless city_keys.blank?  
    cancel_str = "'CancelReasonId':'#{cancel}'," unless cancel.blank?  
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetJobRequest.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{#{id_str if id_str} 'JobRequestReasonId':'#{job_request_reason}', 'ResourceTypeId':'#{resource_type}', 
                    'ResourceId':'#{resource}', 'UserId':'#{current_user.id}', 'CancelAuthorId':'#{current_user.id}', 'DepartmentId':'#{department}', 'Description':'#{descr}', 
                    'JobStartDate':'#{start_date}', 'JobFinishDate':'#{end_date}', #{city_keys_str if city_keys_str} #{cancel_str if cancel_str} 'IsClose': #{is_close}}"
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp['JobRequestId']
    else  
      @error = @resp['ErrorMessage']
      @error += request.body if Rails.env == 'development'
    end
  end
  
  def get_resources_by_type resource_type = nil
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetResourcesByType.ashx#{'?ResourceTypeId=' + resource_type if resource_type}"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
  def get_cities_with_regions
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetCitiesWithRegion.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
    
    # =================WORKS======================================================

  def get_destinations_by_region region
    url = "http://esbsrv01.monopoly.su/ESB/ST/Read/GetDestinations.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{'IsMaintenanceStation':'1',  'RegionKey':'#{region}'}"
    response = http.request(request)
    get_esb_result response 
  end
   

  def get_job_performs
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetJobPerforms.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
   
    
  def set_job_perform id, resource_type, resource, department, reason, destination_key_start, destination_key_end, pl_start_date, pl_end_date, 
                      f_start_date, f_end_date, job_requests, is_failed = false, cancel = nil, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?  
    cancel_str = "'CancelReasonId':'#{cancel}'," unless cancel.blank?  
    job_requests = "[]" if job_requests.blank?  
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetJobPerform.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{#{id_str if id_str} 'ResourceTypeId':'#{resource_type}', 'ResourceId':'#{resource}', 'UserId':'#{current_user.id}', 'CancelAuthorId':'#{current_user.id}', 'DepartmentId':'#{department}', 
                    'JobRequestReasonId':'#{reason}', 'DestinationKeyStart':'#{destination_key_start}', 'DestinationKeyEnd':'#{destination_key_end}',
                    'PlannedDateStart':'#{pl_start_date.to_datetime.to_s(:db) unless pl_start_date.blank?}', 'PlannedDateEnd':'#{pl_end_date.to_datetime.to_s(:db) unless pl_end_date.blank?}', 
                    'FactDateStart':'#{f_start_date.to_datetime.to_s(:db) unless f_start_date.blank?}', 'FactDateEnd':'#{f_end_date.to_datetime.to_s(:db) unless f_end_date.blank?}',
                    'IsClose': #{is_close}, 'IsFailed': #{is_failed}, #{cancel_str if cancel_str} 'JobRequests': #{job_requests} }"
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp#['JobRequestId']
    else  
      @error = @resp['ErrorMessage']
      @error += request.body if Rails.env == 'development'
    end
  end
  
      # ================= CANCELS ======================================================
   
  def get_entry_cancels
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetJobCancelReasons.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
   
  def get_work_cancels
    url = "http://esbsrv01.monopoly.su/ESB/MS/Read/GetJobCancelReasons.ashx?JobPerform=true"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    get_esb_result response
  end
  
      
  def set_entry_cancel id, resource_type, name, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?  
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetJobCancelReason.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{#{id_str if id_str} 'ResourceTypeId':'#{resource_type}', 'Name':'#{name}', 'IsClose': #{is_close}}"
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp#['JobRequestId']
    else  
      @error = @resp['ErrorMessage']
      @error += request.body if Rails.env == 'development'
    end
  end
      
  def set_work_cancel id, resource_type, name, is_close = false
    id_str = "'Id':'#{id}'," unless id.blank?  
    url = "http://esbsrv01.monopoly.su/ESB/MS/Write/SetJobCancelReason.ashx"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = "{#{id_str if id_str} 'ResourceTypeId':'#{resource_type}', 'Name':'#{name}', 'JobPerform':true, 'IsClose': #{is_close}}"
    response = http.request(request)
    @resp = JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    if @resp['Ok']
      @resp#['JobRequestId']
    else  
      @error = @resp['ErrorMessage']
      @error += request.body if Rails.env == 'development'
    end
  end
  
  
  
  private
  
  def show_popup; end
  
end
