class TaskActionAccessesController < ApplicationController
  before_action :set_task_action_access, only: [:show, :edit, :update, :destroy]


  def index
    @task_action_accesses = TaskActionAccess.all
  end

  def show
  end

  def new
    @task_action_access = TaskActionAccess.new
  end

  def edit
  end

  def create
    @task_action_access = TaskActionAccess.new(task_action_access_params)

    if @task_action_access.save
      redirect_to @task_action_access, notice: 'Доступ к действиям с шаблонами задач добавлен'
    else
      render :new
    end
  end

  def update
    if @task_action_access.update(task_action_access_params)
      redirect_to @task_action_access, notice: 'Доступ к действиям с шаблонами задач сохранен'
    else
      render :edit
    end
  end

  def destroy
    @task_action_access.destroy
    redirect_to task_action_accesses_url, notice: 'Доступ к действиям с шаблонами задач удален'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_task_action_access
    @task_action_access = TaskActionAccess.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def task_action_access_params
    params.require(:task_action_access).permit(:task_action_id, :task_template_id, :employee_id, :staff_unit_id, :start_date, :end_date, :is_close)
  end
end
