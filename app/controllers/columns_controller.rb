# -*- encoding : utf-8 -*-

class ColumnsController < ApplicationController


  def index
    @columns = Column.joins(:table).order('tables.name')#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @columns }
    end
  end


  def show
    @column = Column.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @column }
    end
  end


  def new
    @column = Column.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @column }
    end
  end


  def edit
    @column = Column.find(params[:id])
  end


  def create
    @column = Column.new(column_params)

    respond_to do |format|
      if @column.save
        format.html { redirect_to columns_url, notice: 'Column добавлен.' }
        format.json { render json: @column, status: :created, location: @column }
      else
        format.html { render action: "new" }
        format.json { render json: @column.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @column = Column.find(params[:id])

    respond_to do |format|
      if @column.update_attributes(column_params)
        format.html { redirect_to columns_url, notice: 'Column сохранен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @column.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @column = Column.find(params[:id])
    @column.close

    respond_to do |format|
      format.html { redirect_to columns_url }
      format.json { head :no_content }
    end
  end



 private

 def column_params
   params.require(:column).permit(:description, :end, :is_close, :name, :start, :table_id)
 end


end

