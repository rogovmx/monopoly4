class TransportbrandsController < ApplicationController

  def index

    @transportbrands_all = Transportbrand.all
    @letters = @transportbrands_all.map{|t| t.name[0]}.uniq.sort
    if params[:letter].blank? || params[:letter] == 'all'
      @transportbrands = @transportbrands_all.sort_by(&:name)
    else
      @transportbrands = @transportbrands_all.find_all{|t| t.name[0] == params[:letter]}.compact
    end
    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @transportbrand = Transportbrand.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportbrand = Transportbrand.find(params[:id])
  end


  def create
    @transportbrand = Transportbrand.new(transportbrand_params)

    respond_to do |format|
      if @transportbrand.save
        format.html { redirect_to transportbrands_path, notice: 'Марка добавлена' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportbrand = Transportbrand.find(params[:id])

    respond_to do |format|
      if @transportbrand.update_attributes(transportbrand_params)
        format.html { redirect_to transportbrands_url, notice: 'Марка сохранена' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @transportbrand = Transportbrand.find(params[:id])
    @transportbrand.close

    respond_to do |format|
      format.html { redirect_to transportbrands_url, notice: 'Марка удалена' }
    end
  end



 private

 def transportbrand_params
   params.require(:transportbrand).permit(:name, :is_close, :id)
 end


end
