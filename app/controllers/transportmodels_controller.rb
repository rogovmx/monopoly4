class TransportmodelsController < ApplicationController


  def index
    @transportmodels = Transportmodel.visible

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new 
    @transportmodel = Transportmodel.new

    @transportkind = @transportmodel.transportkind
    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportmodel = Transportmodel.find(params[:id])
    @transportkind = @transportmodel.transportkind

  end


  def create
    @transportmodel = Transportmodel.new(transportmodel_params)

    respond_to do |format|
      if @transportmodel.save
        format.html do 
          unless params[:tank].blank?
            @transportmodel.transporttanks.create(params[:tank].values) 
          end  
          
          unless params[:loading].blank?
            params[:loading].values.each do |id|
              @transportloading = Transportloading.find(id['transportloading_id'])
              @transportmodel.transportloadings << @transportloading if @transportloading
            end 
          end  
          redirect_to edit_transportmodel_path(@transportmodel), notice: "Модель сохранена #{params[:v_tank]}" 
        end
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportmodel = Transportmodel.find(params[:id])
    respond_to do |format|
      if @transportmodel.update_attributes(transportmodel_params)
        format.html do 
          unless params[:tank].blank?
            @transportmodel.transporttanks.delete_all
            @transportmodel.transporttanks.create(params[:tank].values) 
          end  
          
          unless params[:loading].blank?
            @transportmodel.transportloadings.delete_all
            params[:loading].values.each do |id|
              @transportloading = Transportloading.find(id['transportloading_id'])
              @transportmodel.transportloadings << @transportloading if @transportloading
            end 
          end  
          redirect_to edit_transportmodel_path(@transportmodel), notice: "Модель сохранена" 
        end
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transportmodel = Transportmodel.find(params[:id])
    @transportmodel.close

    respond_to do |format|
      format.html { redirect_to transportmodels_url( brand: transportbrand ) }
    end
  end

  
  
  # _______________________________________________________________________________________________________________
  
  def add_tank
    @transporttank = Transporttank.new
  end


  def add_loading
    @transportloading = Transportloading.new
  end
  
  
  def chng_attr
   @out = params[:attr]
   respond_to do |format|
      format.js   {}
   end
  end
  
  





 private

 def transportmodel_params
   params.require(:transportmodel).permit(:name, :transportbrand_id, :transportkind_id, :enginemodel_id, :weight, :capacity, :is_engine, :is_loading, :is_fuelling, :id, :is_close,
                  :overall_height, :overall_width, :overall_length, :useful_height, :useful_width, :useful_length, :useful_volume)
 end

end
