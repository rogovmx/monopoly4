class ExtraSpendingTypesController < ApplicationController
  before_action :set_extra_spending_type, only: [:edit, :update, :destroy]

  respond_to :html

  def index
    @extra_spending_types = ExtraSpendingType.all
    respond_with(@extra_spending_types)
  end

  def new
    @extra_spending_type = ExtraSpendingType.new
    respond_with(@extra_spending_type)
  end

  def edit
  end

  def create
    @extra_spending_type = ExtraSpendingType.new(extra_spending_type_params)
    respond_to do |format|
      if @extra_spending_type.save
        format.html { redirect_to extra_spending_types_url, notice: "Тип доп. расходов сохранен" }
      else
        format.html { render action: "new" }
      end
    end  
  end

  def update
    respond_to do |format|
    
      if @extra_spending_type.update(extra_spending_type_params)
        format.html { redirect_to extra_spending_types_url, notice: "Тип доп. расходов сохранен" }
      else
        format.html { render action: "edit" }
      end
    end  
  end

  def destroy
    @extra_spending_type.close
    respond_with(@extra_spending_type) do |format|
      format.html { redirect_to extra_spending_types_url, notice: "Тип доп. расходов удален"}
    end
  end

  private
    def set_extra_spending_type
      @extra_spending_type = ExtraSpendingType.find(params[:id])
    end

    def extra_spending_type_params
      params.require(:extra_spending_type).permit(:name, :is_close)
    end
end
