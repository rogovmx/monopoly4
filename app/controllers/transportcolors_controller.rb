class TransportcolorsController < ApplicationController


  def index
    @transportcolors = Transportcolor.all#.paginate(page: params[:page], per_page: 30)

    respond_to do |format|
      format.html # index.html.erb
    end
  end


  def new
    @transportcolor = Transportcolor.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end


  def edit
    @transportcolor = Transportcolor.find(params[:id])
  end


  def create
    @transportcolor = Transportcolor.new(transportcolor_params)

    respond_to do |format|
      if @transportcolor.save
        format.html { redirect_to transportcolors_path, notice: 'Цвет добавлен' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def update
    @transportcolor = Transportcolor.find(params[:id])

    respond_to do |format|
      if @transportcolor.update_attributes(transportcolor_params)
        format.html { redirect_to transportcolors_path, notice: 'Цвет сохранен' }
      else
        format.html { render action: "edit" }
      end
    end
  end


  def destroy
    @transportcolor = Transportcolor.find(params[:id])
    @transportcolor.close

    respond_to do |format|
      format.html { redirect_to transportcolors_url, notice: 'Цвет удален' }
    end
  end


 private

 def transportcolor_params
   params.require(:transportcolor).permit(:name, :is_close)
 end

end
