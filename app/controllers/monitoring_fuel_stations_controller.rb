class MonitoringFuelStationsController < ApplicationController

  
  def refuellings
    @from = params[:from].blank? ? Time.now : params[:from].to_time
    @to = params[:to].blank? ? Time.now : params[:to].to_time
    @from, @to = @to, @from if @from > @to
    
    @from_par = DateTime.new(@from.year, @from.month, @from.day, 0, 0, 0).to_time
    @to_par = DateTime.new(@to.year, @to.month, @to.day, 23, 59, 59).to_time
    
    
    @refuellings = Azs.refuelling(@from_par.to_i, @to_par.to_i) #all data
    @coord_names = @refuellings.map { |r| r["AuthorName"].try(:strip) }.compact.uniq
    @azs_types = @refuellings.map { |r| r["AZSType"].try(:strip) }.compact.uniq.sort
    @azs_names = @refuellings.map { |r| r["AZSName"].try(:strip) }.compact.uniq.sort
    @azs_types_price = Hash[*@refuellings.map { |r| [r["AZSType"].try(:strip), r["AZSPrice"].to_f ] }.compact.uniq.flatten]
    @total_litres = @refuellings.inject(0){|sum,x| sum + x["Litres"].to_f }.round(2)
    
    #-----------
    #Согласования координаторов
    @coordinators = []
    @coord_names.each do |c_n|
      @coordinators << [c_n,  @refuellings.find_all { |r| r["AuthorName"] == c_n }.size ]
    end
    #-----------
    #Литраж по тягачу и типу АЗС
    @trucks_drivers = @refuellings.map { |r| [r["RegNumber"].try(:strip), r["DriverName"].try(:strip) ] }.compact.uniq
    @trucks_by_azs_litres = []
    @trucks_drivers.each do |t_d|
      @tmp_litres_by_azs = []
      @tmp_litres_by_azs_no_unv = []
      @tmp_litres_by_azs_price = []
      @tmp_not_agree = []
      @azs_types.each do |azs| 
        ref = @refuellings
          .find_all { |r| r["RegNumber"].try(:strip) == t_d[0] && 
            r["DriverName"].try(:strip) == t_d[1] && 
            r["AZSType"].try(:strip) == azs}
        @tmp_not_agree << ref.find_all { |r| r["AgreeResult"].try(:strip) != 'Согласовано'}.inject(0){|sum,x| sum + x["Litres"].to_f } 
        @tmp_litres_by_azs << ref.inject(0){|sum,x| sum + x["Litres"].to_f } 
        @tmp_litres_by_azs_no_unv <<  ref.inject(0){|sum,x| sum + x["Litres"].to_f } unless azs == 'не определен'
        @tmp_litres_by_azs_price << (@tmp_litres_by_azs_no_unv.last * @azs_types_price[azs]) unless azs == 'не определен'
      end
      @tmp_litres_by_azs_price_sum = @tmp_litres_by_azs_price.sum.round(2)
      @tmp_litres_sum = @tmp_litres_by_azs.sum.round(2)
      @tmp_litres_sum_no_unv = @tmp_litres_by_azs_no_unv.sum.round(2)
      @tmp_not_agree_sum = @tmp_not_agree.sum.round(2)
      @trucks_by_azs_litres << [t_d[0], t_d[1]] + @tmp_litres_by_azs.map { |t| "#{t.round(2)} (#{(t*100/@tmp_litres_sum).round(2)}%)" } + [@tmp_litres_sum] + [@tmp_not_agree_sum]  + [(@tmp_litres_by_azs_price_sum / @tmp_litres_sum_no_unv).round(2)]
    end
    #-----------
    #Согласованный коорд. литраж по типам АЗС
    @coordinators_by_azs_litres = []
    @coord_names.each do |c_n|
      @tmp_litres_by_azs = []
      @tmp_litres_by_azs_no_unv = []
      @tmp_litres_by_azs_price = []
      @quantity = 0
      @azs_types.each do |azs| 
        ref = @refuellings.find_all { |r| r["AuthorName"].try(:strip) == c_n && r["AgreeResult"].try(:strip) == 'Согласовано' && r["AZSType"].try(:strip) == azs}
        @tmp_litres_by_azs <<  ref.inject(0){|sum,x| sum + x["Litres"].to_f } 
        @quantity += ref.size
        @tmp_litres_by_azs_no_unv <<  ref.inject(0){|sum,x| sum + x["Litres"].to_f }  unless azs == 'не определен'
        @tmp_litres_by_azs_price << (@tmp_litres_by_azs_no_unv.last * @azs_types_price[azs]) unless azs == 'не определен'
      end
      @tmp_litres_by_azs_price_sum = @tmp_litres_by_azs_price.sum.round(2)
      @tmp_litres_sum = @tmp_litres_by_azs.sum.round(2)
      @tmp_litres_sum_no_unv = @tmp_litres_by_azs_no_unv.sum.round(2)
      @coordinators_by_azs_litres << [c_n] + @tmp_litres_by_azs.map { |t| "#{t.round(2)} (#{(t*100/@tmp_litres_sum).round(2)}%)" } + [@tmp_litres_sum]+ [(@tmp_litres_by_azs_price_sum / @tmp_litres_sum_no_unv).round(2)] + [@quantity]
    end
    #-----------
    #Литраж по типам АЗС
    @azs_litres = []
    @azs_types.each do |azs|
      @tmp_litres = @refuellings.find_all { |r| r["AZSType"].try(:strip) == azs}.inject(0){|sum,x| sum + x["Litres"].to_f }
      @azs_litres << [azs, @tmp_litres.round(2), (@tmp_litres*100/@total_litres).round(2)]
    end
    #-----------
    #Литраж по АЗС
    @azs = []
    @azs_names_litres = @refuellings.map { |r| r["AZSName"].try(:strip) if r['Litres'].to_f > 0 }.compact.uniq.sort
    @azs_names_litres.each do |azs|
      @tmp_litres = @refuellings.find_all { |r| r["AZSName"].try(:strip) == azs }.inject(0){|sum,x| sum + x["Litres"].to_f }
      @azs << [azs, @tmp_litres.round(2), (@tmp_litres*100/@total_litres).round(2)]
    end
    
  end
  
  def info_by_azs_name
    @from = params[:from].blank? ? Time.now : params[:from].to_time
    @to = params[:to].blank? ? Time.now : params[:to].to_time
    @from, @to = @to, @from if @from > @to
    @from_par = DateTime.new(@from.year, @from.month, @from.day, 0, 0, 0).to_time
    @to_par = DateTime.new(@to.year, @to.month, @to.day, 23, 59, 59).to_time
    @azs_name = params[:azs_name]
    
    @refuellings = Azs.refuelling(@from_par.to_i, @to_par.to_i) 
    @azs_info = @refuellings.find_all { |r| r["AZSName"].try(:strip) == @azs_name && r['Litres'].to_f > 0 }
    @azs_info.map! { |info| [info['RegNumber'], info['MomentRefuelling'].try(:to_datetime), 
                            info['AgreeResult'], info['AgreeMoment'].try(:to_datetime), info['Litres'], info['DriverName'],
                            info['AuthorName'], info['AZSPrice'].try(:to_f)] }
    @azs_info_col_names = ['Тягач', 'Момент заправки', 'Статус', 'Момент согласования', 'Литров', 'Водитель', 'Координатор', 'Стоимость']                    

  end
  
  
  def expensive_refuellings
    @from = params[:from].blank? ? Time.now : params[:from].to_time
    @to = params[:to].blank? ? 1.day.ago : params[:to].to_time
    @from, @to = @to, @from if @from > @to
    
    @from_par = DateTime.new(@from.year, @from.month, @from.day, 0, 0, 0).to_time
    @to_par = DateTime.new(@to.year, @to.month, @to.day, 23, 59, 59).to_time
    
    
    @table_data = get_table_data("http://esbsrv01.monopoly.su/esb/Reports/RefuellingExpensiveCheckCoord.ashx?from=#{@from_par.to_i}&to=#{@to_par.to_i}")
    
  end

  def index
    user_session[:mf_from] = params[:from]
    user_session[:mf_to] = params[:to]

    
    @from = user_session[:mf_from] || Date.today 
    @to =  user_session[:mf_to] || Date.today

    @fillup = MonitoringFuelStations.check_fuel_stations(@from.to_time.to_i, @to.to_time.to_i)

    respond_to do |format|
      format.html # index.html.erb
      format.js { }
    end
  end

  def save_xls
    @from = user_session[:mf_from] || Date.today 
    @to =  user_session[:mf_to] || Date.today
  
    @fillup = MonitoringFuelStations.check_fuel_stations(@from.to_time.to_i, @to.to_time.to_i)

    respond_to do |format|
      format.xls {
        connections = Spreadsheet::Workbook.new
        list = connections.create_worksheet name: 'unavailable_resources'
        list.row(0).concat @fillup.first.keys
        @fillup.each_with_index { |unv, i| list.row(i+1).push *unv.values  }
        header_format = Spreadsheet::Format.new color: :green, weight: :bold
        list.row(0).default_format = header_format
        #output to blob object
        blob = StringIO.new("")
        connections.write blob
        #respond with blob object as a file
        send_data blob.string, type: :xls, filename: "Мониторинг_заправок_ТС_#{Time.now.to_i.to_s}.xls"
        }
    end
  end
  
  
  
  private

  class Azs < Monosql  
    def self.refuelling from, to
      connection.select_all("exec [MonopolySunTemp].[dbo].[ms_AZS_Refuelling_Detail_Get] @MomentFrom = #{from} , @MomentTo = #{to}").to_a
    end
  end

  def monitoring_fuel_stations_params
    params.require(:monitoring_fuel_stations).permit(:moment_from, :moment_to)
  end
  
 
  class MonitoringFuelStations < Monosql
    def self.check_fuel_stations(moment_from, moment_to, for_detail = 1)
      connection.select_all("exec [MonopolySun].[dbo].[ms_AZS_Refuelling_Get] @MomentFrom = #{moment_from}, @MomentTo = #{moment_to}, @ForDetail=#{for_detail}").to_a
    end
  end 
   
end
