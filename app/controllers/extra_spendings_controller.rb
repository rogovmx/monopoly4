class ExtraSpendingsController < ApplicationController
  before_action :set_extra_spending, only: [:show, :edit, :update, :destroy]

  respond_to :html, :js

  def payments
    
    respond_to do |format|
      format.html {@drivers = Person.working_drivers}
      format.js do 
        @res = get_payments params[:driver], params[:start_date], params[:end_date]
        if @res['Ok']
          @payments = @res['Payments']
          @total = @res['Total']
        else 
          @error = @res["Message"]
        end
      end
    end
  end
  
  
  def index
    @from = (user_session[:extra_spendings_from] || 7.days.ago).to_date
    @to = (user_session[:extra_spendings_to] || Time.now).to_date
    @extra_spendings = ExtraSpending.where(created_at: @from.to_date..@to.to_date).includes(:person, :transport, :extra_spending_type)
    respond_with(@extra_spendings)
  end

  def set_date
    user_session[:extra_spendings_from] = (params[:from] || 7.days.ago).to_date
    user_session[:extra_spendings_to] = (params[:to]  || Time.now).to_date
    redirect_to extra_spendings_url
  end
  
  
  def show
    respond_with(@extra_spending)
  end

  def new
    @extra_spending = ExtraSpending.new
    respond_with(@extra_spending)
  end

  def edit
    @res = get_payments @extra_spending.person_id, 10.days.ago, Time.now 
    if @res['Ok']
      @payments = @res['Payments']
      @total = @res['Total']
    else 
      @error = @res["Message"]
    end
  end

  def create
    @extra_spending = ExtraSpending.new(extra_spending_params)
    respond_to do |format|
      if @extra_spending.save
        format.html { redirect_to extra_spendings_url, notice: "Доп. расход сохранеен" }
      else
        format.html { render action: "new" }
      end
    end  
  end

  def update
    respond_to do |format|
      if @extra_spending.update(extra_spending_params)
        format.html { redirect_to extra_spendings_url, notice: "Доп. расход сохранен" }
      else
        format.html { render action: "edit" }
      end
    end  
  end

  def destroy
    @extra_spending.close
    respond_with(@extra_spending) do |format|
      format.html { redirect_to extra_spendings_url, notice: "Доп. расход удален"}
    end
  end
  
  def get_truck_and_payments
    person = params[:person_id]
    employee = Employee.visible.where(person_id: person).limit(1)
    td = TruckDriver.chained.where(employee_id: employee.first.id).limit(1).first if employee
    @truck = td.transport_id if employee && td
    @res = get_payments person, 10.days.ago, Time.now 
    if @res['Ok']
      @payments = @res['Payments']
      @total = @res['Total']
    else 
      @error = @res["Message"]
    end
    #render js: "$('#extra_spending_transport_id').val('#{truck}');"
  end
  

  
  
  
  private
  
    def get_payments driver, start_date, end_date
      snils = Person.find(driver).try(:snils)
#      snils = "040-104-622 78"
#      start_date = "1404172800"
#      end_date = "1406764800"
      url = "http://esbsrv01.monopoly.su/esb/UU/Payments.ashx"
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      parameters =  {'snils' => "#{snils}", 'startTime' => "#{start_date.to_time.to_i}", 'endTime' => "#{end_date.to_time.to_i}"}
      request.set_form_data(parameters)
      response = http.request(request)

      JSON.parse(response.body.to_s.force_encoding(Encoding::UTF_8))
    end
  
  
    def set_extra_spending
      @extra_spending = ExtraSpending.find(params[:id])
    end

    def extra_spending_params
      params[:extra_spending][:user_id] = current_user.id
      params[:extra_spending][:transport_id] = nil if params[:extra_spending][:transport_id].blank?
      params.require(:extra_spending).permit(:person_id, :transport_id, :extra_spending_type_id, :spending_sum, :is_urgently, :is_required_payment, :user_id, :agreement_id, :agreement_result, :agr_text, :agr_date, :payment_request_id, :is_request_status, :request_error, :agreement_request_id, :request_result, :request_date, :agreed_sum, :is_close, :descr)
    end
end
